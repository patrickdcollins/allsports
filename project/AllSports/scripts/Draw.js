
var enabled = false;

function toggle() {
    enabled = !enabled;
    
    var color = CI.CellByName("drawColor").Color;
    
    var r = (color & 0x0000FF);
	var g = (color & 0x00FF00) >> 8;
	var b = (color & 0xFF0000) >> 16;
	
	r /= 255;
	g /= 255;
	b /= 255;
    
    color = [r, g, b];
    
    var cl = TM.createCommandList();
    cl.set("draw/ch_canvas",  "Clear",   true);
    cl.set("draw/canvas",     "Visible", enabled);
    cl.set("draw/tools/Draw", "Color",   color);
    
    // Change button colors
    if (enabled) {
        cl.set("bottom_menu/draw_button1/TAB", "Color", [1, 1, 0]);
        cl.set("bottom_menu/draw_button2/TAB", "Color", [1, 1, 0]);
    } else {
        cl.set("bottom_menu/draw_button1/TAB", "Color", [1, 1, 1]);
        cl.set("bottom_menu/draw_button2/TAB", "Color", [1, 1, 1]);
    }
    
    primaryTitle.execute(cl);
}

function off() {
    if (enabled) {
        toggle();
    }
}
