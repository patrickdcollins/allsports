
var DataEngine = function(bucket, localDataDirectory, host, port) {
    this.request = function(url, async, asText, callback) {
        var xmlhttp;
        try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP.6.0"); } catch (e) {}
        try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP.3.0"); } catch (e) {}
        try { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");  } catch (e) {}

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status == 200) {
                    if (asText) {
                        callback(null, xmlhttp.responseText);
                    } else {
                        callback(null, xmlhttp.responseXML);
                    }
                    xmlhttp = null;
                } else {
                    callback('Remote url: ' + url + ' failed, status: ' + xmlhttp.status);
                    xmlhttp = null;
                }
            }
        }
        
        var sep = (url.indexOf('?') > -1) ? '&' : '?';
        var u = url + sep + 'dummhy=' + new Date().getMilliseconds();
        xmlhttp.open('GET', u, async);
        xmlhttp.send(null);
    }

    this.ds = new ActiveXObject("HegoDataEngine.DataStoreConnection");
    if (host) { this.ds.host = host; }
    if (port) { this.ds.port = port; }
    
    this.localDataDirectory = "";
    if (localDataDirectory) { this.localDataDirectory = localDataDirectory; }

    this._bucket = bucket;
    
    this.createBucket = function(bucket) {
        var url = "http://localhost:4300/_data/_bucket_config/" + bucket;
        this.request(url, false, true, function(status, data) {
            // Requesting the bucket will create it if it doesn't exist
        });
    }
    this.createBucket(this._bucket);
    
    this.getKeys = function(bucket) {
        bucket = typeof(bucket) == "undefined" ? this._bucket : bucket;
    
        var url  = "http://localhost:4300/_data/" + bucket + "/?keysOnly=true";
        var keys = [];
        this.request(url, false, true, function(status, data) {
            data = JSON.parse(data);
            for (var i = 0; i < data.length; i++) {
                keys.push(data[i].key);
            }
        });
        return keys;
    }

    this._get = function(key, async, callback) {
        var req = this.ds.createRequest();
        req.Get(bucket, key, async, function(res, contentType, err, statusCode) {
            if (err) {
                if (callback) { callback(null, {msg: err, status: statusCode}); }
            } else {
                if (contentType == 'application/json') {
                    try {
                        res = JSON.parse(res);
                    } catch (e) {
                    }
                }

                if (callback) { callback(res); }
            }
        });
    }

    this.getSync = function(key, callback) {
        this._get(key, false, callback);
    }

    this.getAsync = function(key, callback) {
        this._get(key, true, callback);
    }

    this.read = function(key) {
        res = null;
        this.getSync(key, function(data, err) {
            if (!err) {
                res = data;
                return;
            }
        });
        if (typeof(res) == "string") {
            try {
                return JSON.parse(res);
            } catch (e) {
                return res;
            }
        } else {
            return res;
        }
    }

    this.write = function(key, value) {
        var res = false;
        this.putSync(key, value, function(err) {
            if (!err) {
                res = true;
                return;
            }
            throw 'DataEngine Error writing key: ' + key + ': ' + err.msg;
        });
        return res;
    }

    this._put = function(key, value, async, callback) {
        var contentType = 'text/plain';

        if (typeof value == 'object') {
            contentType = 'application/json';
            value = JSON.stringify(value);
        }

        var req = this.ds.createRequest();

        req.Put(bucket, key, value, contentType, async, function(err, statusCode) {
            if (err) {
                if (callback) { callback({msg: err, status: statusCode}); }
            } else {
                if (callback) { callback(); }
            }
        });
    }

    this.putSync = function(key, value, callback) {
        return this._put(key, value, false, callback);
    }

    this.putAsync = function(key, value, callback) {
        this._put(key, value, true, callback);
    }

    this._delete = function(key, async, callback) {
        var req = this.ds.createRequest();
        req.Delete(bucket, key, async, function(err, statusCode) {
            if (err) {
                if (callback) { callback({msg: err, status: statusCode}); }
            } else {
                if (callback) { callback(); }
            }
        });
    }

    this.deleteAsync = function(key, callback) {
        this._delete(key, true, callback);
    }

    this.deleteSync = function(key, callback) {
        return this._delete(key, false, callback);
    }

    this.registerEvents = function(events, callback) {
        for (e in events) {
            this.ds.addEventSubscription(events[e].bucket, events[e].key, function(event) {
                if (callback) { callback({connected: true, msg: event}); }
            });
        }
    }
    
    this.backup = function() {
        var keys = this.getKeys();
        var fso = new ActiveXObject("Scripting.FileSystemObject");
        for (var i = 0; i < keys.length; i++) {
            var filePath = this.localDataDirectory;
            var fileName = keys[i] + ".json";
            var jsonData = JSON.stringify(this.read(keys[i]));
            
            if (!fso.FolderExists(filePath)) { fso.CreateFolder(filePath); }
            
            CGCFile.Clear();
            CGCFile.Text = jsonData;
            CGCFile.Save(filePath + "\\" + fileName);
        }
    }
    
    this.restore = function() {
        if (this.localDataDirectory == "") { return; }
        
        var fso = new ActiveXObject("Scripting.FileSystemObject");
        if (!fso.FolderExists(this.localDataDirectory)) { return; }
        
        var folder = fso.GetFolder(this.localDataDirectory);
        for (var files = new Enumerator(folder.Files); !files.atEnd(); files.moveNext()) {
            var item = files.item();
            var name = item.Name;
            
            CGCFile.Clear();
            CGCFile.Load(this.localDataDirectory + "\\" + name);
            
            var obj = JSON.parse(CGCFile.Text);
            var key = name.split(".")[0];
            
            this.write(key, obj);
        }
    }
}
