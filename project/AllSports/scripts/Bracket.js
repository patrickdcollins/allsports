
function initialize() {
    var cl = TM.createCommandList();
    cl.beginTransaction();
    cl.doAction("Show_bracket");
    primaryTitle.execute(cl);
}

function deinitialize() {
    var cl = TM.createCommandList();
    cl.beginTransaction();
    // cl.doAction("Hide_bracket");
    primaryTitle.execute(cl);
}

// transition is called from the bracket.gse scene via Client.send

function transition(transitionName) {
    var cl = TM.createCommandList();
    cl.doAction(transitionName);
    primaryTitle.execute(cl);
}
