--Lightweight attributeHandler class

require("CHYRONHEGO_LIB_StringUtils.lua");


AttributeHandler = {};

function AttributeHandler:new (o)
      o = o or {}   -- create object if user does not provide one
      setmetatable(o, self)
      self.__index = self
      
      o.attr = {};
      o.attributeChanged = nil;
      return o
    end



local internalAttributeChange = false;
function AttributeHandler:onAttrChange(attrs)
	--print( 'in attrChange in attribute handle' );
	for i = 1, #attrs do
		--print( 'attr = ', attrs[i] );
	end;
	function change(attribute)
    		local changed = false;
    		if self.attr[attribute:firstCharLower()] ~= Scene.get(attribute) then
      			changed = true;
      			self.attr[attribute:firstCharLower()] = Scene.get(attribute);
    		end;
    		return changed;
  	end;
  	--======================================
	if internalAttributeChange == true then internalAttributeChange = false; return; end;
	if self.attributeChanged == nil then return; end;
	local changed = {};

	for _,attr in pairs(attrs) do
		if attr == 'Reset' then 
			--print( 'RESET value = ', self.attr.reset );	
		end;
		if change(attr) then 
			table.insert(changed,attr);		
		end;
	end;
	  
	for _, attr in pairs(changed) do
		if self.attributeChanged then
		  self.attributeChanged(attr, Scene.get(attr));	
		end;
	end;
    
end;

function AttributeHandler:setAttr(attribute, value)
	internalAttributeChange = true;
	Scene.set(attribute, value);	
	self.attr[attribute:firstCharLower()] = value; 
end;

function AttributeHandler:init(attrs)
	for attr,value in pairs(attrs) do
	  self.attr[attr] = value;
	end;
end;



local divider = '-------------------------------------------------------------';
function AttributeHandler.help()
	print('');
	print(divider);
	print('AttributeHandler');
	print( 'Use:');
	print( '  Create an instance of this class and create an anonymous function to receive the ');
	print( '  Events.onAttrChange event. In this function call the onAttrChange function of this class');
	print( '  Create an callbackmethod to the instance and assign it the class attributeChanged handle to');
	print( '  receive the filtererd/changed attributes');
	print( '  Ex: ');
	print( '  local attrHandler = AttributeHandler:new();');
	print( '  function onAttrChange(attribute, value)');
	print( '      --handle the attribute change here');
	print( '  end;');
	print( '');
	print( '  Events.onAttrChange = function(attrs)');
	print( '      attrHandler:onAttrChange(attrs);');
	print( '  end;');
	print( ' ');
	print('Methods:');
	print('   setAttr(attr, value)');
	print('      sets the attribute value without triggering a new onAttrChange event');
	print('');
	print('   init({attr = value, .. })');
	print('      initialize attributes. ');
	print('');
	print( divider );
end;
