
function initialize() {
    var cl = TM.createCommandList();
    cl.loadScene("bottom_menu.gse", "bottom_menu");
    cl.loadScene("draw.gse", "draw");
    cl.beginTransaction();
    cl.doAction("Show_bottom_menu");
    primaryTitle.execute(cl);
}

function deinitialize() {
    Draw.off();
    
    var cl = TM.createCommandList();
    cl.beginTransaction();
    cl.doAction("Hide_bottom_menu");
    cl.beginTransaction();
    cl.deleteNode("bottom_menu");
    cl.deleteNode("draw");
    primaryTitle.execute(cl);
}
