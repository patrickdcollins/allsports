
var TM;
var primaryTitle;
var secondaryTitle;
var GS2Host;
var dataEngine;
var navigation;

var projectFolder = "D:\\projects\\AllSports";

try {
    var wshShell = new ActiveXObject("WScript.Shell");
    var wshEnv   = wshShell.Environment("USER");
    GS2Host      = wshEnv("GS2HOST");
    dataDir      = wshEnv("GS2DATADIR");
} catch (e) {
    log("Error trying to initialize globals -> " + e);
}

try {
    launchAllSports();
} catch (e) {
    log("Error in Main.launchAllSports -> " + e);
}

try {
    initializeProject();
} catch (e) {
    log("Error in Main.initializeProject -> " + e);
}

navigate("splash");

function launchAllSports() {
    var path = projectFolder + "\\software\\AllSports.exe";
    var objShell = new ActiveXObject("WScript.shell");
    
    var killCmd = "taskkill /im \"AllSports.exe\"";
    
    objShell.run(killCmd);
    objShell.run(path);
}

function initializeProject() {
    TM = new ActiveXObject("GSTk5.TitleManager");
    TM.setProject("AllSports");
    
    var client = TM.getClient();
    client.setServerAddress(GS2Host);
    client.connect();
    client.onMessageListener = Client_onMessage;
    client.roaSet("::Output", "mousePickOverride", true);
    
    primaryTitle = TM.createTitle("default");
    secondaryTitle = TM.createTitleInChannel("cleanFeed", 1);
    
    var cl = TM.createCommandList();
    cl.loadScene("clean_feed.gse", "clean_feed");
    secondaryTitle.execute(cl);
    
    var code   = "";
    var objXML = new ActiveXObject("Microsoft.XMLDOM");
    objXML.load(projectFolder + "\\compass\\nav.xml");
    
    var xmlRoot  = objXML.documentElement;
    var xmlCodes = xmlRoot.selectNodes("Code");
    var xmlCode;
    while ((xmlCode = xmlCodes.nextNode()) != null) {
        code += xmlCode.text;
    }
    navigation = eval(code);
    
    dataEngine = new DataEngine.DataEngine("AllSports");
    
    var events = [{
        "bucket" : "AllSports",
        "key"    : "*"
    }];
    dataEngine.registerEvents(events, dataEngine_onEvent);
}

function navigateToStartPage() {
    navigate("bracket");
}

function log(msg) { CILog.Insert(0, msg); }

function Client_onMessage(msg) {
    var cmd = msg.split("|");
    switch (cmd[0]) {
        case "navigate":
            navigate(cmd[1], cmd[2]);
            break;
        case "eval":
            eval(cmd[1]);
            break;
    }
}

function navigate(page, param) {
    param = param || null;
    try {
        navigation.go(page, param);
    } catch (e) {
        log("Error in Main.navigate -> " + e);
    }
}

function dataEngine_onEvent(event, msg) {
    var key = JSON.parse(event.msg).key;
    var keyJSON = JSON.stringify(dataEngine.read(key));
    var cl = TM.createCommandList();
    cl.set(key + "/api", "Data", keyJSON);
    primaryTitle.execute(cl);
}
