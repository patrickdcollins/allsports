local mt = getmetatable(string) or {};
mt.__index = mt;
mt.__newindex = mt;

mt.firstCharUpperRestLower = function(self)
  return self:sub(1,1):upper()..self:sub(2):lower();
end; 

mt.firstCharUpper = function(self)
  return self:sub(1,1):upper()..self:sub(2);
end; 

mt.firstCharLower = function(self)
  return self:sub(1,1):lower()..self:sub(2);
end; 

mt.mcIfy = function(self)
  if self:sub(1,2) == 'MC' then
    return self:sub(1,1):upper()..self:sub(2,2):lower()..self:sub(3):upper()
  else
    return self:sub(1):upper()
  end;
end;

mt.split = function(self, separator)
  local ary = {};
  local iCount = 1;
  for key, value in string.gmatch(self, "(%w+)"..separator.."(%w+)") do
    ary[iCount] = {key,value};
    iCount = iCount + 1;
  end;
  return ary;
end;

setmetatable(string, mt);
 
