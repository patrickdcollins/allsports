
function initialize() {
    var bracketData = dataEngine.read("bracket");
    
    var cl = TM.createCommandList();
    
    cl.beginTransaction();
    cl.loadScene("background.gse", "background");
    cl.loadScene("splash.gse", "splash");
    
    cl.beginTransaction();
    cl.deleteNode("bracket");
    cl.loadScene("bracket.gse", "bracket");
    cl.set("bracket/api", "Data", JSON.stringify(bracketData));
    
    cl.beginTransaction();
    cl.deleteNode("loading");
    cl.doAction("Show_splash");
    
    primaryTitle.execute(cl);
}

function deinitialize() {
    var cl = TM.createCommandList();
    cl.deleteNode("splash");
    primaryTitle.execute(cl);
}
