require("LUA/TouchUtilities.lua");
require("LUA/NodeUtilities.lua");
require("LUA/Constants.lua");
--TODO  
-- * take origin into account.. now it assumes a 0.5/0.5 origin of the group
-- * if changing expandtype while expanded, then the list gets out of synch.. 









-- this OO aproach might be somewhat strange but I wanted a structure that enables inheritance.. but at the 
--   same time prioritize speed and keep memory usage as	 low as I can.. 
-- Therefore inheritance is not really inheritance.. instead it copies the content of the class it inherits..
-- The reason for this is to avoid a large metatable structure that force a deep search to find the methods.. 
-- To save memory usage I apply "almost" the same principle for the instances of the class.. but with the difference that
-- functions will not be copied... allowing all instance to share the same methods.. 
-- If you find flaws in this.. or know about at better way .. please inform me.. 
-- /Michael Lekrans 2012-06-25

function instanceCopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) == 'function' then return; end;
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end
    return _copy(object)
end

function classCopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end
    return _copy(object)
end

Object = {
	new = function(self)
		   o = instanceCopy(self);
		   setmetatable(o, {__index = self});
                   o.inherit = function(self, inheritFrom)
				c = classCopy(inheritFrom);
				for attribute, value in pairs(c) do
                                  self[attribute] = value;                                  
				end;                                                                  
			       end;
		   o.whoAreYou = 'undefined';
		   return o;
		 end;
		   
}


function class(superclass)
  superclass = superclass or Object;
  local c = {}
  if superclass then
    c = classCopy(superclass);
  end;
  return c;
end;




--==========================================================================
--                           PICKTAG
--==========================================================================

PickTag = class();
PickTag.whoAreYou = 'PickTag';
PickTag.name = '';
PickTag.tag = '';
PickTag.node = nil;

--==========================================================================
--                           PICKTAGS
--==========================================================================
PickTags = class()

-- membervariables
PickTags.topLevelOnly = true;
PickTags.tags = {};
PickTags.whoAreYou = 'PickTags';
-- memberfunctions
PickTags.add = function(self, pickTag)
  if self.tags[pickTag.tag] then 
    printError('PickTags: pickTag.tag already exists: '..pickTag.tag);
    return
  else 	
    self.tags[pickTag.tag] = pickTag; 
  end;
end;
			
PickTags.clear = function(self)
			    self.tags = {};
			  end;
			  

PickTags.tagExistsInTouch = function(self, topLevelOnly, tagList)
  local exists = false;
  if tagList then    
    for _, tagsObj in pairs(tagList) do
      if tagsObj then 
        if (topLevelOnly and (tagsObj.top)) or (not topLevelOnly) then
          for _, tag in pairs(tagsObj.tags) do 
            if self.tags[tag] then 
              exists = true; 
              break;
            end;
          end;
        end;
      end;
    end;
  end  
  return exists;
end;

-- initializations

--==========================================================================
--                           TOUCHRECEIVER
--==========================================================================
TouchReceiver = class() 

-- membervariables
TouchReceiver.dispatcher = nil;
TouchReceiver.pickTagEffects = PickTags:new();
TouchReceiver.touchID = -1; 


-- memberfunctions
TouchReceiver.onTouch = function(self, touches)  
  if not self.dispatcher then return; end;
  
  for _, touch in pairs(touches) do 
    if self:validateTouch(touch) then
      self:dispatchTouch(touch);
    end;
  end;
end;
				    
TouchReceiver.validateTouch = function(self, touch)
  valid = false;	
  if touch.type == Events.DOWN then 
   if self.pickTagEffects:tagExistsInTouch(self.attr.topPickOnly, touch.pick) then
      valid = true; 
    end;                                 
  elseif touch.id == self.touchID then
    valid = true;
  end;
  return valid;
end;

TouchReceiver.dispatchTouch = function(self, touch)
  if touch.type == Events.DOWN then
    self.touchID = touch.id;
    self.dispatcher:_onTouchDown(touch);
  elseif touch.type == Events.MOVE then
    self.dispatcher:_onTouchMove(touch);
  elseif touch.type == Events.UP then
    self.dispatcher:_onTouchUp(touch);
    self.touchID = -1;
  else 
    error('not a valid touch event in Touchreceiver.dispatchTouch');
  end;						  
end;
						
-- initializations
TouchReceiver.pickTagEffects.topLevelOnly = true;

--==========================================================================
--                           TOUCHDISPATCHER
--==========================================================================

TouchDispatcher = class();
TouchDispatcher.touchProcessOrderOnDown = DISPATCHER_PROCESSORDER_AFTER_MAIN;
TouchDispatcher.touchProcessOrderOnMove = DISPATCHER_PROCESSORDER_AFTER_MAIN;
TouchDispatcher.touchProcessOrderOnUp = DISPATCHER_PROCESSORDER_AFTER_MAIN;
TouchDispatcher.listeners = {};

TouchDispatcher.registerListener = function(self, touchDispatcher)
  table.insert(self.listeners, touchDispatcher);
end;


TouchDispatcher._onTouchDown = function(self, touch)
  function dispatch(order)
    for _, listener in ipairs(self.listeners) do
      if listener.touchProcessOrderOnDown == order then
        listener:onTouchDown(touch);
      end;
    end;
  end;
  --========================
  dispatch(DISPATCHER_PROCESSORDER_FIRST);
  dispatch(DISPATCHER_PROCESSORDER_BEFORE_MAIN);
  self:onTouchDown(touch);
  dispatch(DISPATCHER_PROCESSORDER_AFTER_MAIN);
  dispatch(DISPATCHER_PROCESSORDER_LAST);
end;


TouchDispatcher._onTouchMove = function(self, touch)

  function dispatch(order)
    for _, listener in ipairs(self.listeners) do
      if listener.touchProcessOrderOnMove == order then
        listener:onTouchMove(touch);
      end;
    end;
  end;
  --========================
  dispatch(DISPATCHER_PROCESSORDER_FIRST);
  dispatch(DISPATCHER_PROCESSORDER_BEFORE_MAIN);
  self:onTouchMove(touch);
  dispatch(DISPATCHER_PROCESSORDER_AFTER_MAIN);
  dispatch(DISPATCHER_PROCESSORDER_LAST);
end;


TouchDispatcher._onTouchUp = function(self, touch)
  function dispatch(order)
    for _, listener in ipairs(self.listeners) do
      if listener.touchProcessOrderOnUp == order then
        listener:onTouchUp(touch);
      end;
    end;
  end;
  --========================
  dispatch(DISPATCHER_PROCESSORDER_FIRST);
  dispatch(DISPATCHER_PROCESSORDER_BEFORE_MAIN);
  self:onTouchUp(touch);
  dispatch(DISPATCHER_PROCESSORDER_AFTER_MAIN);
  dispatch(DISPATCHER_PROCESSORDER_LAST);
end;


TouchDispatcher.onTouchDown = function(self, touch)
                                             printError('Abstract OnTouchDown in '..self.whoAreYou);
					   end;							
TouchDispatcher.onTouchMove = function(self, touch)
                                             printError('Abstract OnTouchMove in '..self.whoAreYou);
					   end;							
TouchDispatcher.onTouchUp = function(self, touch)
                                             printError('Abstract OnTouchUp in '..self.whoAreYou);
					   end;		



--==========================================================================
--                           TOUCHCOMPONENT
--==========================================================================
TouchComponent = class(TouchDispatcher)
TouchComponent.Active = false;
TouchComponent.targetStartPos = nil;
TouchComponent.parentStartPos = nil;
TouchComponent.nodes = {};
TouchComponent.objects = {};
TouchComponent.whoAreYou = 'TouchComponent';
TouchComponent.targetMoveAllowed = true;
TouchComponent.selfNode = Scene.this();
TouchComponent.parent = nil;
TouchComponent.target = Scene.node(); --default use parent to script as target
TouchComponent.internalAttributeChange = false;

TouchComponent.touchInfo = {
  node = nil,
  touchDown = nil,
  lastTouch, nil,
  id = -1,
  touch_down_callback = nil,
  touch_move_callback = nil,
  touch_up_callback = nil, 
  offsetBetweenTouchYAndTargetPosY = 0,
  offsetBetweenTouchXAndTargetPosX = 0
}

TouchComponent.init = function(self)
  --abstract init. Override this inheriting class.. 
end;

TouchComponent.onTouchDown = function(self, touch)
  self:_initTouchInfo(touch);	
  self.targetStartPos = self:getTargetPos();
  self.parentStartPos = self:getParentPos();	
  self.pickStartPos = self:getPickPos(touch);  
  self:processTouchDown(touch);
end;

TouchComponent.onTouchMove = function(self, touch)
  self:_updateTouchInfo(touch);
  self:processTouchMove(touch);
end;


TouchComponent.onTouchUp = function(self, touch)
  self:processTouchUp(touch);
  self:_resetTouchInfo();
end;

TouchComponent.processAttributeChange = function(self)
  --abstract.. override in component class
end;

TouchComponent.processOnEveryFrame = function(self)
  --abstract.. override in component class
end;


TouchComponent.processTouchDown = function(self, touch)
						      --abstract.. override this in component class
						    end;					  

TouchComponent.processTouchMove = function(self, touch)
						      --abstract.. override this in component class
						    end;					  
TouchComponent.processTouchUp = function(self, touch)
						      --abstract.. override this in component class
						    end;	

TouchComponent._initTouchInfo = function(self, touch)
  self.touchInfo.id = touch.id;
  self.touchInfo.touchDown = touch;
  self.touchInfo.lastTouch = touch;
  self.touchInfo.offsetBetweenTouchXAndTargetPosX  = convertTouchToPos(touch)[POS_X] - self:getTargetPos()[POS_X];
  self.touchInfo.offsetBetweenTouchYAndTargetPosY  = convertTouchToPos(touch)[POS_Y] - self:getTargetPos()[POS_Y];
end;

TouchComponent.isActive = function(self)
  printError('abstract isACtive in TouchComponent: Override this in inheriting class');
end;

TouchComponent._updateTouchInfo = function(self, touch)
  self.touchInfo.lastTouch = touch;
end;

TouchComponent._resetTouchInfo = function(self)
  self.touchInfo.id = -1;
  self.touchInfo.touchDown = nil;
  self.touchInfo.lastTouch = nil;
end;

TouchComponent.getPickPos = function(self, touch)
  return {Scene.WIDTH*touch.x,Scene.HEIGHT*touch.y};
end;

TouchComponent.getTargetPos = function(self)
  return Scene.get(self.target, 'Position');
end;

TouchComponent.getParentPos = function(self)
  return Scene.get(Scene.node(), 'Position');
end;

TouchComponent.setTargetPos = function(self, pos)
  if self.targetMoveAllowed then
    Scene.set(self.target, 'Position', pos);
  else
    printError( 'NOTE! Trying to change targetposition when targetMoveAllowed = false ', self.whoAreYou );
  end;
end;


TouchComponent.convertTouchToTargetPos = function(self, touch)
  local currentTouchY = touch.y * Scene.HEIGHT;
  local currentTouchX = touch.x * Scene.WIDTH;
  local pos = {}
  pos[1] = currentTouchX - self.touchInfo.offsetBetweenTouchXAndTargetPosX;
  pos[2] = currentTouchY - self.touchInfo.offsetBetweenTouchYAndTargetPosY;
  return pos;
end;

TouchComponent.getAttribute = function(self, attribute)
  local status, value = pcall(Scene.get, self.selfNode, attribute:firstCharUpper());
  if status then 
    return value
  else
    return nil; 
  end;  
end;

TouchComponent.setAttribute = function(self, attribute, value)
  self.internalAttributeChange = true;
  Scene.set(self.selfNode, attribute:firstCharUpper(), value);
end;
						    				  
TouchComponent.processMessage = function(self, msg)
  msg = translateMessage(msg);
  if msg ~= '' then 
    Client.send(msg);
  end;
end;
	

--==========================================================================
--                           EVENTDISPATCHER
--==========================================================================
EventDispatcher = class();

EventDispatcher.onEveryFrameListeners = {};
EventDispatcher.onAttributeChangeListeners = {};

count = 0;
EventDispatcher.onEveryFrame = function(self)
  for _,listener in ipairs(self.onEveryFrameListeners) do
    listener:processOnEveryFrame();
  end;
end;

EventDispatcher.onAttributeChange = function(self)
  for _,listener in ipairs(self.onAttributeChangeListeners) do
    listener:processOnAttributeChange();
  end;
end;

EventDispatcher.registerOnEveryFrameListener = function(self, newListener)
  if not newListener then 
    printError('EventDispatcher.registerOnEveryFrameListener. Failed to register listener. Listener is null');
  end;
  for _, registeredListener in ipairs(self.onEveryFrameListeners) do
    if newListener == registeredListener then
      error('ERROR: EventDispatcher: Listener aldready registered in registerOnEveryFrameListener', 2);
      return;
    end;
  end;
  table.insert(self.onEveryFrameListeners, newListener);
end;

EventDispatcher.registerOnAttributeChangeListener = function(self, newListener)
  for _, registeredListener in ipairs(self.onAttributeChangeListeners) do
    if newlistener == registeredListener then
      error('ERROR: EventDispatcher: Listener aldready registered in registerOnAttributeChangeListener', 2);
      return;
    end;
  end;
  table.insert(self.onAttributeChangeListeners, newListener);
end;


--==========================================================================
--                           CLASSINSTANCES
--==========================================================================

ClassInstanceContainer = class();
ClassInstanceContainer.instances = {}
ClassInstanceContainer.registerInstance = function(self, className, instance)
  if not self.instances[className] then
    self.instances[className] = instance;
  end;
end; 

--==========================================================================
--                           TOUCH CONTROLLER
--==========================================================================
Controller = class();



