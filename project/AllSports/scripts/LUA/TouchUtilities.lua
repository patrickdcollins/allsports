require("LUA/Constants.lua");
require("LUA/NodeUtilities.lua");
--dofile "d:\\projects\\HEGO_touch_components\\scripts\\HEGO_constants.lua";

--TODO refactor parseSearchString and parseMsgString


function isTouchDown(touch)
  return touch.type == Events.DOWN;
end;

function isTouchMove(touch)
  return touch.type == Events.MOVE;
end;

function isTouchUp(touch)
  return touch.type == Events.UP;
end;

function touchTypeToString(touchType)
  if touchType == Events.DOWN then return 'EVENT touch-down: '; end;
  if touchType == Events.MOVE then return 'EVENT touch-move: '; end;
  if touchType == Events.UP then return 'EVENT touch-up: '; end;
  return 'unknown touchtype';
end;


function split(str, pat)
    local t = {} 
    local fpat = "(.-)" .. pat
    local last_end = 1
    local s, e, cap = str:find(fpat, 1)
    while s do
      if s ~= 1 or cap ~= "" then
	 table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
    end
    if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
    end
    return t
end

function convertTouchToPos(touch)
  if not touch then error( 'Error: convertTouchToPos. touch is nil', 2); end;
  local x = touch.x;
  local y = touch.y;
  x = x * Scene.WIDTH;
  y = y * Scene.HEIGHT;
  return {x, y, touch.z};    
end;



function searchForNode(searchInfo)
  function getSearchNode(startNode, path)
    local node = startNode;
    local currentIndex = 1; 
    if path[1] == SEARCH_ROOT_TOKEN then
      node = getRootNode(startNode);
      currentIndex = 2;
    else      
      while path[currentIndex] == SEARCH_PARENT_TOKEN do
        node = Scene.parent(node);
        currentIndex = currentIndex + 1;
      end;      
    end;
    for i = currentIndex, #path do
      node = Scene.find(node, path[i]);
    end;
    
    return node;
  end;
  --===================================
  local node = nil;
  local searchNode = getSearchNode(searchInfo.startNode, searchInfo.path);
  if searchNode then 
    if not searchInfo.name then
      node = searchNode
    else
      node = Scene.find(searchNode, searchInfo.name);
      if not node and searchInfo.autoFind then 
        for i = 1, SEARCH_SUFFIX_COUNT do
          node = Scene.find(searchNode, searchInfo.name .. i);
          if node then 
            break;
          end;
        end;
      end;
    end;  
  end;
  return node;
end;


function parseSearchString(searchString)
  function extractToken(s, token, searchDirection)    
    local t = nil;
    if searchDirection == SEARCH_FROM_START then
      if string.sub(s, 1, #token) == token then
        t = token;
        s = string.sub(s, #token + 1); 
      end;
    else
      if string.sub(s, (#s - #token) + 1, #s) == token then
        t = token;
        s = string.sub(s, 1, #s - #token); 
      end;
    end;
    return t, s;
  end;
  
  function tokenize(s)
    local tokens = {};
    if not s or s == '' then return tokens; end;
    local t, s = extractToken(s, SEARCH_ROOT_TOKEN, SEARCH_FROM_START);
    if t then 
      table.insert(tokens, t);
    else
      t, s = extractToken(s, SEARCH_PARENT_TOKEN, SEARCH_FROM_START);
      while t do 
        table.insert(tokens, t);
        t, s = extractToken(s, SEARCH_PARENT_TOKEN, SEARCH_FROM_START);
      end;
    end;

    local parentNames = split(s, SEARCH_NODE_SEPARATOR_TOKEN);
    for _, name in ipairs(parentNames) do
      if name ~= '' then
        table.insert(tokens, name);
      end;
    end;
    return tokens;
  end;

  function isName(s)
    return (s ~= SEARCH_ROOT_TOKEN) and (s ~= SEARCH_PARENT_TOKEN);
  end;

  function extractSuffix(s)
    local suffix, s = extractToken(s, SEARCH_MATCH_ALL_TOKEN, searchDirection);
    return suffix, s; 
  end;
  --==========================================

 
  local searchInfo = {
    startNode = nil,
    path = nil,
    name = nil,
    autofind = nil,
  }

  local s = searchString;
  local tokens = tokenize(s);
  local lastToken = tokens[#tokens];

  if lastToken == SEARCH_MATCH_ALL_TOKEN then
    searchInfo.name = lastToken;
    searchInfo.autoFind = true;
    tokens[#tokens] = nil;
  elseif isName(lastToken) then 
    local suffix = nil;
    suffix, searchInfo.name = extractSuffix(lastToken);   
 
    if suffix == SEARCH_MATCH_ALL_TOKEN then
      searchInfo.autoFind = true;
    end;

    tokens[#tokens] = nil;
  end;
  
  searchInfo.path = tokens;  
  return searchInfo;
end;


function findNode(startNode, searchString, autoFind, defaultName)
  if Scene.type(startNode) == 'Script' then
    startNode = Scene.parent(startNode);
  end;
  
  if not searchString or searchString == '' then
    if not defaultName or defaultName == '' then
      --only happens when we want current node
      if Scene.type(startNode) ~= 'Group' then 
        return Scene.parent(startNode)
      else
        return startNode;
      end;
    end;
    if autoFind then
      searchString = defaultName..'*';
    else
      return nil; 
    end;
  end;

  searchInfo = parseSearchString(searchString); 
  
  searchInfo.startNode = startNode;
  if not searchInfo.name or searchInfo.name == '' or searchInfo.name == SEARCH_MATCH_ALL_TOKEN then
    searchInfo.name = defaultName;
    searchInfo.autoFind = true;
  end;
  return searchForNode(searchInfo);
end;

function extractAttribute(str)
  --search for first occurance of non reserved char or non alfabetical or non numerical (like spaces, pipes, commas, dashes and so on)
  -- in searchstrings, backslash and dots are used to define searchPaths.. 
  -- ^%a = no character
  -- ^%d = no number
  -- ^%// = no backslash
  -- ^%. = no dots
  local start, stop = string.find(str, '[^%a^%d^%\\^%.]');
  if not start then 
    return str, ''
  else
    return str:sub(1, start-1), str:sub(start);
  end;
end;


function translateMessage(msg)
  local attr = '';
  local str = '';
  local start, stop = msg:find('@'); 
  
  while start do
    str = str .. msg:sub(1, start - 1);
    attr, msg = extractAttribute(msg:sub(start + 1));    
    local searchInfo = parseMsgString(attr);
    local path = '';
    attr = searchInfo.name;
    print( 'searchInfo.path = ', searchInfo.path);
    for i = 1, #searchInfo.path do
      print( 'i = ', i, ' path = ', path)
      path = path .. searchInfo.path[i] .. SEARCH_NODE_SEPARATOR_TOKEN;
    end;
    local node = nil;
    if path == '' then
      node = Scene.this()
    else 
      node = findNode(Scene.node(), path, false) or Scene.this();
    end;
    if attr == 'Name' then
      local attrValue = Scene.name(node);
      str = str .. attrValue;
    else
      local status, attrValue = pcall(Scene.get, node, attr);
      if not status then 
        print('ERROR: ' .. Scene.name(Scene.parent(node)) .. '/' .. Scene.name(node) .. ' has no attribute: ' .. attr);
      else
        str = str .. tostring(attrValue);
      end;    
    end;
    print( 'node = ', Scene.name(node));
    
    start, stop = msg:find('@');
  end;
  str = str .. msg;
  return str;
end;



function parseMsgString(searchString)
  function extractToken(s, token, searchDirection)    
    local t = nil;
    if searchDirection == SEARCH_FROM_START then
      if string.sub(s, 1, #token) == token then
        t = token;
        s = string.sub(s, #token + 1); 
      end;
    else
      if string.sub(s, (#s - #token) + 1, #s) == token then
        t = token;
        s = string.sub(s, 1, #s - #token); 
      end;
    end;
    return t, s;
  end;
  
  function tokenize(s)
    local tokens = {};
    if not s or s == '' then return tokens; end;
    local t, s = extractToken(s, SEARCH_ROOT_TOKEN, SEARCH_FROM_START);
    if t then 
      table.insert(tokens, t);
    else
      t, s = extractToken(s, SEARCH_PARENT_TOKEN, SEARCH_FROM_START);
      while t do 
        table.insert(tokens, t);
        t, s = extractToken(s, SEARCH_PARENT_TOKEN, SEARCH_FROM_START);
      end;
    end;

    local parentNames = split(s, SEARCH_NODE_SEPARATOR_TOKEN);
    for _, name in ipairs(parentNames) do
      if name ~= '' then
        table.insert(tokens, name);
      end;
    end;
    return tokens;
  end;

  function isName(s)
    return (s ~= SEARCH_ROOT_TOKEN) and (s ~= SEARCH_PARENT_TOKEN);
  end;

  function extractSuffix(s)
    print( 'extractSuffix s = ', s );
    local suffix, s = extractToken(s, SEARCH_MATCH_ALL_TOKEN, searchDirection);
    return suffix, s; 
  end;
  --==========================================

 
  local searchInfo = {
    startNode = nil,
    path = nil,
    name = nil,
    autofind = nil,
  }

  local s = searchString;
  local tokens = tokenize(s);
  local lastToken = tokens[#tokens];

  if isName(lastToken) then 
    local suffix = nil;
    suffix, searchInfo.name = extractSuffix(lastToken);   
     tokens[#tokens] = nil;
  end;
  
  searchInfo.path = tokens;  
  return searchInfo;
end;


function getAnim(s, e, dur, dest)
  local anim = { 
    {
      keyframes = {
        { 0, s, "SMOOTH", "FLAT" },
        { dur, e, "FLAT"}
      },
      destination = dest
    }
  }
  return anim;
end

getAttribute = function(attribute)
  local status, value = pcall(Scene.get, Scene.this(), attribute:firstCharUpper());
  if status then 
    return value
  else
    return nil; 
  end;  
end;

function oppositeAxis(axis)
  if axis == HORIZONTAL then
    return VERTICAL
  else 
    return HORIZONTAL;
  end;
end;



function printError(errorText)
  print('');
  print( '====  NOTE!!: ', errorText, ' ====');
  print('');
  --error('ERROR: '..errorText, 3);
end;


function attentionPrint(str)
  local nodeName = Scene.name(Scene.node()) .. '\\' .. Scene.name(Scene.this()) .. ': ';
  print( '-----------------------------------');
  print( 'ATTENTION: ' .. nodeName .. str);
  print( '-----------------------------------');
end;

function printTable(t)
  print( '' );
  for attribute, value in pairs(t) do
    print( attribute, ' = ', value );
  end;
  print( '' );
end;

function hsvToRGB(h,s,v)
  local r,g,b;
  local i;
  local f,p,q,t;
                                         
  h = 360 * h;

  if (s == 0) then
    r = v;
    g = v;
    b = v;
    return {r,g,b};
  end;  
  
  h = h / 60;
  i = math.floor(h);
  f = h - i;
  p = v * (1 - s);
  q = v * (1 - s * f);
  t = v * (1 - s * (1 - f));
  
    if i == 0 then
         r = v;
         g = t;
         b = p;
    elseif i == 1 then 
         r = q;
         g = v;
         b = p;
    elseif i == 2 then 
         r = p;
         g = v;
         b = t;
    elseif i == 3 then 
         r = p;
         g = q;
         b = v;
    elseif i == 4 then 
         r = t;
         g = p;
         b = v;
    else
      r = v;
      g = p;
      b = q;
    end;
                                           
  return {r,g,b}      
end;                                

