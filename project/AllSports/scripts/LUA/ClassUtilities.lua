require("LUA/StringUtilities.lua");
require("LUA/Constants.lua");

function getMoveStabilizerNode(callingNode, searchString, autoFind)
  return findNode(callingNode, searchString, autoFind, MOVE_STABILIZER.NODE_NAME);
end;

function getInertiaNode(callingNode, searchString, autoFind)
  return findNode(callingNode, searchString, autoFind, INERTIA.NODE_NAME);
end;


function getRubberbandNode(callingNode, searchString, autoFind)
  return findNode(callingNode, searchString, autoFind, RUBBERBAND.NODE_NAME);
end;


function getGroupArrangeNode(callingNode, searchString, autoFind)
  return findNode(callingNode, searchString, autoFind, GROUP_ARRANGE.NODE_NAME);
end;

function getSnapNode(callingNode, searchString, autoFind)
  return findNode(callingNode, searchString, autoFind, SNAP.NODE_NAME);
end;


function getFreehandStroke(callingNode, searchString, autoFind)  
  return findNode(callingNode, searchString, autoFind, 'freehandStroke');
end;

function getMoveBoundaryNode(callingNode, searchString, autoFind)
  -- look for alternative extensions here... 
  local node = nil;
  node = getScriptNodeFromAttributeOrDefaultName('MoveBoundaryScript', 'moveBoundaryTopHighBottomLow', attr);
  if node == nil then
    node = getScriptNodeFromAttributeOrDefaultName('MoveBoundaryScript', 'moveBoundaryTopLowBottomHigh', attr);
  end;
  return node;
end;


function classNameToScriptNodeName(attr)
  local scriptName = '';
  for _, node in pairs(NODE) do
    if node.CLASS_NAME == attr then
      scriptName = node.NODE_NAME;
      break;
    end;
  end;
  return scriptName;  
end;



function getPickTag(callingNode, searchString, autoFind)
  return findNode(callingNode, searchString, autoFind, PICKTAG_NODE_NAME);  
end;


function getImageNode(callingNode, searchString, autoFind)
  return findNode(callingNode, searchString, autoFind, 'image');
end;

