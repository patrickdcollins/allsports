-- library
-- Hego 2011

-- Node Utilities. Helpfunctions for managing and manipulate nodes
--===========================================================================

--[[

  function getRootNode(node):rootNode
	* RETURNS the rootNode of the node tree

  function screenPos(node):pos
	* Descr: Calculates the actual screenposition of an node and return the position as
    		an array.

    	* RETURNS: Screenposition of node as an array: pos[1]=x, pos[2]=y, pos[3]=z

  
  function screenScale(node):scale
	* Descr: Calculates the actual Scale of an node as it is percieved, taken into account 
		all parentgroups scale, eg. an image with scale = 1 inside a group with scale = 0.5 
        	inside another group with scale = 0.5 will return the scale = 0.25

    	* RETURNS: ScreenScale of node as an array: scale[1]=x, scale[2]=y, scale[3]=z


  
  function traverseToRoot(node, <searchRes>, <callback>)
  	* Descr: Traverses node downto the rootnode and add the node and it's parentnodes to searchRes
		and calls callback (if supplied) with node and searchRes for each node.
	
	- Node: 	Node (group,image,clip etc).
	- searchRes: 	Optional (can be nil). Array to store nodes
	- callback:	Optional. Callback function <function (node,searchRes)>. Gets called for each
			node in the traverse.

	* RETURNS: 	Nothing



  function getNodeSize(node, visibleContentOnly):size
  	* Descr: Return the size of the node by traversing the nodes children and estimate the size
	  	by it's visible content. 
		If "visibleContentOnly" = true it will return the size of it's visible content otherwise
		it will include it's own originpoint to the calculation (eg the node's position is outside
                the area with the visible content)
       		
	
	- Node: 	Node (group,image,clip etc).
	- visibleContentOnly: 	Optional (can be nil). Array to store nodes

    	* RETURNS: Size of node as an array: size[1]=width, size[2]=height



--]]

-- PROTOTYPES
-- -----------------------------------------------------------------------
-- -----------------------------------------------------------------------

-- RECT ---

Rect = {}
Rect.prototype = {left = 0, top = 0, right = 0, bottom = 0}
Rect.mt = {}

function Rect.new(o)
  setmetatable(o, Rect.mt);
  return o;
end

Rect.mt.__index = function(table, key)
  return Rect.prototype[key];
end


-- POSITION ---

Position = {}
Position.prototype = {x = 0, y = 0, z = 0}
Position.mt = {}

function Position.new(o)
  setmetatable(o, Position.mt);
  return o;
end

Position.mt.__index = function(table, key)
  return Position.prototype[key];
end


-- SCALE ---

Scale = {}
Scale.prototype = {x = 1, y = 2, z = 3}
Scale.mt = {}

function Scale.new(o)
  setmetatable(o, Scale.mt);
  return o;
end

Scale.mt.__index = function(table, key)
  return Scale.prototype[key];
end



-- FUNCTIONS
-- -----------------------------------------------------------------------
-- -----------------------------------------------------------------------

function getRootNodeOld(node) 
  while Scene.parent(node) ~= nil do
    node = Scene.parent(node);
  end
  return node
end


function screenPos(node)
-- returns the screenposition of node
  local property = {};
  local tmpProp = {};
  local nodes = {}
  local scale = {};
  nu_initTable(property,4,0);

  traverseToRoot(node, nodes);

  for i=0, #nodes-1 do
    scale = nu_getParentScale(nodes[i]);
    if scale ~= nil then
      tmpProp = Scene.get(nodes[i], 'Position');
      property = nu_addProperty(property, tmpProp, true);
      for i = 1, 3 do
        property[i] = property[i]*scale[i];
      end
    end      
  end    
  return property;
end;




function screenScale(node)
-- Returns the screenscale of node
  local property = {};
  local nodes = {}
  nu_initTable(property,4,1);

  traverseToRoot(node, nodes);

  for i=0, #nodes-1 do
    property = nu_multiplyProperty(property, Scene.get(nodes[i], 'Scale'), true);
  end  

  return property;
end;


function traverseToRoot(node, searchRes, callback)
  local ix = 0;
  while node ~= nil do
    searchRes[ix] = node;
    if callback ~= nil then 
      callback(node,searchRes); 
    end;
    ix = ix + 1;
    node = Scene.parent(node);       
  end
end 



function getNodeSize(node, visibleContentOnly)
-- if node is a node with size (image, clip) or it can be calculated (text) .. then return
-- the size as it is
-- otherwise (node that contains visible content) then calculate it

  local size = nu_getSize(node);
  if size ~= nil then 
    return size;
  end;

  size = {};
   
 
  local rect = nu_getNodeRect(node);
  
  nu_initTable(size, 3, 0);

  if (visibleContentOnly ~= nil) and (visibleContentOnly == false) then
    -- include the node's left and bottom if they are outside the visible content
    local pos = Scene.get(node, 'Position');
    if rect.left > pos[1] then rect.left = pos[1];end;
    if rect.bottom > pos[2] then rect.bottom = pos[2];end;
  end

  -- compensate for the scale of the node (the size value should not be 
  -- affected by scale) and return the size

  local scale = Scene.get(node, 'Scale'); 
  if rect ~= nil then
    size[1] = (rect.right - rect.left) * (1/scale[1]);
    size[2] = rect.top - rect.bottom * (1/scale[2]);
  end;

  return size;
end




function getChildrenOfTypeCount(parentNode, type, prefix)
  local count = 0;
  local node = Scene.firstChild(parentNode);
  while node do
    if Scene.type(node) == type and Scene.get(node, 'Visible') then
      if prefix then
         if Scene.name(node):sub(1,#prefix) == prefix then
           count = count + 1;
         end;
      else 
        count = count + 1;
      end;
    end;
    node = Scene.next(node);
  end;  
  
  return count;  
end;


function getChildrenOfType(parentNode, type, prefix)
  local children = {};
  local node = Scene.firstChild(parentNode);
  while node do
    if Scene.type(node) == type and Scene.get(node, 'Visible') then
      if prefix then
         if Scene.name(node):sub(1,#prefix) == prefix then
            table.insert(children, node);
         end;
      else 
        table.insert(children, node);
      end;
    end;
    node = Scene.next(node);
  end;  
  
  return children;  
end;

local maxDepth = 15;
function parentByName(parentName)
  local parentNode;
  local currentNode = Scene.node();
  for i = 1, maxDepth do
    if currentNode then
      if Scene.name(currentNode) == parentName then
        parentNode = currentNode;
        break;
      end;
    else
      break;
    end;
    currentNode = Scene.parent(currentNode);
  end;
  return parentNode;
end;


--  ***************************
--  UTILITY FUNCTIONS
--  ***************************

function nu_getOrigin(node)
  local type = Scene.type(node);
  local origin = {};
  nu_initTable(origin, 3, 0);  
 
  if ((type == 'Image') or (type == 'Clip')) then
    return Scene.get(node, 'Origin' );
  elseif type == 'Text' then 
    return;
  else 
    -- calculate origin based on it's visual content and the 0.0 offset from that content
    return;
  end
  
end

function nu_getSize(node)
-- return the nodes size as an array if the node has a size property or it can be calculated
-- ToDo .. calculate a textnode size

  local type = Scene.type(node);

  if ((type == 'Image') or (type == 'Clip')) then
    if Scene.get(node, 'Visible') then
      return Scene.get(node, 'Size' );
    else 
      return nil;
    end;
  elseif type == 'Text' then 
    return nil;
  else 
    return nil;
  end
end

function nu_initTable(t, count, defaultValue)
  for i=0, count-1 do
    t[i] = defaultValue;
  end
end;

function nu_getParentScale(node)
  local scale = 1; 
  node = Scene.parent(node);
  while node ~= nil do
    scale = Scene.get(node, 'Scale');
    if scale ~= nil then
      return scale;
    else
      scale = 1;
    end;      
    node = Scene.parent(node);
  end
end

function nu_subtractProperty(prop1, prop2, isTable)
  if prop1 == nil then return prop1;end;
  if prop2 == nil then return prop1;end;

  if isTable == true then
    if #prop1 < #prop2 then return prop1;end;
    for i=1, #prop2 do
      prop1[i] = prop1[i] - prop2[i];  
    end;
  else
    prop1 = prop1 - prop2;
  end;
  return prop1;
end


function nu_addProperty(prop1, prop2, isTable)
  if prop1 == nil then return prop1;end;
  if prop2 == nil then return prop1;end;

  if isTable == true then
    if #prop1 < #prop2 then return prop1;end;
    for i=1, #prop2 do
      prop1[i] = prop1[i] + prop2[i];  
    end;
  else
    prop1 = prop1 + prop2;
  end;
  return prop1;
end


function nu_multiplyProperty(prop1, prop2, isTable)
  if prop1 == nil then return res;end;
  if prop2 == nil then return res;end;
  if isTable == true then
    for i=1, #prop2 do
      prop1[i] = prop1[i] * prop2[i];  
    end;
  else
    prop1 = prop1 * prop2;
  end;
  return prop1;
end


function nu_getNodeRect(node)
-- calculate a nodes visible area as percieved on the screen, not the actual size. 
-- Eg. an image with scale = 1 inside a parent with scale = 0.5 has a percieved area
-- that is half its actual size.

-- traverse children recursively upwards and try to find nodes that has a size
-- when found, get the screenpos and screenscale to get left,top,right,bottom values

-- TODO take into account Clipplane and such
  local rect = nil; 
  local pos = nil;

  -- get size of node. If node doesn't have size it returns nil
  local size = nu_getSize(node);

  if size ~= nil then 
    pos = screenPos(node);
    scale = screenScale(node);
 
    local origin = nu_getOrigin(node);
    local width = size[1] * scale[1];
    local height = size[2] * scale[2];
    


    local offsX = width * origin[1];
    local offsY = height * origin[2];    
    
    rect = Rect.new{};

    rect.left = pos[1] - offsX;
    rect.bottom = pos[2] - offsY;
    rect.right = rect.left + width;
    rect.top = rect.bottom + height;
    
    -- return because a node with size don't have children
    return rect;
  else
    -- traverse children
    
    local childRect = nil;
    local child = Scene.firstChild(node);
    
    while child ~= nil do 
      childRect = nu_getNodeRect(child);
      if childRect ~= nil then
        if rect == nil then 
          rect = childRect;
        else 
          if childRect.left < rect.left then rect.left = childRect.left; end;
	  if childRect.right > rect.right then rect.right = childRect.right; end;
          if childRect.top > rect.top then rect.top = childRect.top; end;
          if childRect.bottom < rect.bottom then rect.bottom = childRect.bottom; end;
        end;
      end;
      child = Scene.next(child)
    end
    return rect;
  end
end;


function screenPosToLocal(node, screenPosition, index )
  local pos = Scene.get(node, 'Position');
  local localScreenPos = screenPos(node);
  
  if index then
    pos[index] = screenPosition + (pos[index] - localScreenPos[index]);
    return pos[index]
  else
    pos[POS_X] = screenPosition[POS_X] + (pos[POS_X] - localScreenPos[POS_X]);
    pos[POS_Y] = screenPosition[POS_Y] + (pos[POS_Y] - localScreenPos[POS_Y]);
    return pos;
  end;

end;




--==================================================================
function getRootNode(node)
  local node = node;
  local parent = Scene.parent(node);
  
  while parent ~= nil do
    node = parent;
    if Scene.name(node) == 'scene' then break; end;
    parent = Scene.parent(node);
  end;
  return node;  
end;


function findAllScriptNodes()
  nodeList = {};
  local node = Scene.node();
  local rootNode = getRootNode(node);
  nodeList = getScriptNodes(rootNode, nodeList);
  return nodeList;
end;

function findAllSubTargetsScriptsByID(targetID)
  nodeList = {};
  local node = Scene.node();
  local rootNode = getRootNode(node);
  nodeList = getScriptNodes(rootNode, nodeList);

  local subTargets = {};
  for _,node in pairs(nodeList) do 
    local status, id = pcall(Scene.get, node, 'TargetID');
    if status and id == targetID then
      table.insert(subTargets, node);
    end;
  end;
  return subTargets;
end;

function findAllMagnetsByGroupID(groupID)
  nodeList = {};
  local node = Scene.node();
  local rootNode = getRootNode(node);
  nodeList = getScriptNodes(rootNode, nodeList);

  local magnets = {};
  for _,node in pairs(nodeList) do 
    local status, groups = pcall(Scene.get, node, 'MagnetGroups');
    if status then
      if groups ~= '' then
        local groupArray = split(groups, ',');
        for i = 1, #groupArray do
          if tonumber(groupArray[i]) == tonumber(groupID) then
            if Scene.get(node, 'Enable') then
              table.insert(magnets, node);          
            end;
          end;
        end;
      end;
    end;
  end; 
  return magnets;
end;


function findAllRadioButtonScriptsByID(radioGroupID)
  nodeList = {};
  local node = Scene.node();
  local rootNode = getRootNode(node);
 
  nodeList = getScriptNodes(rootNode, nodeList);

  local radioButtons = {};
  for _,node in pairs(nodeList) do 
    local status, id = pcall(Scene.get, node, 'RadioGroupID');
    if status and id == radioGroupID then
      table.insert(radioButtons, node);
    end;
  end;
  return radioButtons;
end;


function findAllSubPickTagScriptsByID(targetID)
  nodeList = {};
  local node = Scene.node();
  local rootNode = getRootNode(node);
  nodeList = getScriptNodes(rootNode, nodeList);

  local subPickTags = {};
  for _,node in pairs(nodeList) do 
    local status, id = pcall(Scene.get, node, 'PickID');
    if status and id == targetID then
      table.insert(subPickTags, node);
    end;
  end;
  return subPickTags;
end;


function getScriptNodes(rootNode, nodeList)

  local childNode = Scene.firstChild(rootNode);
  
  while childNode ~= nil do     
    nodeList = scriptNodesInNode(childNode, nodeList);
    nodeList = getScriptNodes(childNode, nodeList);
    childNode = Scene.next(childNode);
  end;  
  return nodeList;
end;


function scriptNodesInNode(node, nodeList)
  local nodeEffect = Scene.firstEffect(node);
  while nodeEffect ~= nil do     
    if Scene.type(nodeEffect) == 'Script' then
      table.insert(nodeList, nodeEffect);
    end;
    nodeEffect = Scene.next(nodeEffect);
  end;
  return nodeList;
end;


function setUniquePickTag(pickTagNode)
  local tag = Scene.get(pickTagNode, 'Tag');
  local tagAry = split(tag, '-');
  
  if tagAry[1] == PICKTAG_UNIQUE_PREFIX then 
    return;
  end;
  
  local newTag = PICKTAG_UNIQUE_PREFIX .. '-' .. Scene.name(Scene.parent(pickTagNode)) .. '-' .. tostring(math.random());
  Scene.set(pickTagNode, 'Tag', newTag);  
end;