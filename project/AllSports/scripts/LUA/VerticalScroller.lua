--TODO 
-- sometimes inertia does not start
require("LUA/Classes.lua");
require("LUA/TouchUtilities.lua");
require("LUA/NodeUtilities.lua");
require("LUA/Constants.lua");
require("LUA/StringUtilities.lua");

--==========================================================================
--                           INERTIA CLASS
--==========================================================================
Inertia = class(TouchComponent);
Inertia.whoAreYou = 'Inertia Component';
Inertia.target = Scene.node();

Inertia.active = true;
Inertia.objects = {};
Inertia.inertiaAllowed = true;
Inertia.abortInertia = false;
Inertia.objects.snap = nil;
Inertia.minPos = nil;
Inertia.maxPos = nil;
Inertia.bounceAreaEnteredVertical = nil; 

Inertia.snapPointList = {};
Inertia.inProgress = false;

Inertia.attr = {
  attenuation = 0.95,
  bounce = true,
  bounceDistance = 30,
  minVelocity = 0.001,
  bounceAnimDuration = 20,
  snap,
  useSnap = true,
}
Inertia.inertia = {
  MIN_VELOCITY = 0.0000001,
  ATTENUATION_CALC_INC_STEP = 0.00005,
  attenuationCalculated = false,  
  attenuation = 0,
  velocity = 0,
  running,
}

Inertia.velocityCalculationInfo = {
  id = -1;
  y = 0;
  dy = 0;
  touchEvents = {};
  velocity = 0;
}

Inertia.bounceAreaEnteredPos = nil;


Inertia.init = function(self)
  self.whoAreYou = 'inertia instance';
  if classInstanceContainer then
    self.objects.snap = classInstanceContainer.instances[NODE.SNAP.CLASS_NAME];
  end;
  self:readAttributes();
end;


Inertia.readAttributes = function(self)
  function change(attribute)
    local changed = false;
    if self.attr[attribute:firstCharLower()] ~= Scene.get(self.selfNode, attribute) then
      changed = true;
      self.attr[attribute:firstCharLower()] = Scene.get(self.selfNode, attribute);
    end;
    return changed;
  end;
  --======================================    
  change('Attenuation');

end;


Inertia.processOnEveryFrame = function(self)
  self:processInertia();
end;

Inertia.processTouchDown = function(self, touch)
  if (not self.target) or (not self.active) then return; end;
  self:stopInertia();
  self:initVelocityCalculation(touch);
  self.inertia.running = false;
  self.inProgress = false;
end;

Inertia.processTouchMove = function(self, touch)
  if (not self.target) or (not self.active) then return; end;
  self:updateVelocity(touch);
end;


Inertia.processTouchUp = function(self, touch)
  if (not self.target) or (not self.active) then return; end;
  self:initInertia(touch);
end;

Inertia.stopInertia = function(self)
  self.inertia.started = false;
  self.inProgress = self.inertia.started;
  self.abortInertia = false;  
end;


Inertia.resetInertia = function(self)
  self.bounceAreaEnteredPos = nil;
end;


Inertia.initVelocityCalculation = function(self, touch)
  self.velocityCalculationInfo.id = touch.id;
  self.velocityCalculationInfo.y = touch.y;
  self.velocityCalculationInfo.dy = 0;
  self.velocityCalculationInfo.touchEvents = {};
  self.velocityCalculationInfo.velocity = 0;
end

Inertia.updateVelocity = function(self, touch) 
  self:addTouchPointToInternalTouch(touch); 
  self:updateVelocityToInternalTouch();
end

Inertia.addTouchPointToInternalTouch = function(self, touch)
  self.velocityCalculationInfo.y = touch.y;
  table.insert(self.velocityCalculationInfo.touchEvents, {x=touch.x, y=touch.y, time=touch.time});
end;

Inertia.updateVelocityToInternalTouch = function(self)
  local fromEvent = #self.velocityCalculationInfo.touchEvents - TOUCH_EVENT_COUNT_FOR_VELOCITY_CALC;
  
  if (fromEvent < 1) then 
    fromEvent = 1; 
  end;  
  local toEvent = #self.velocityCalculationInfo.touchEvents - EXCLUDE_LAST_N_TOUCH_EVENTS_FOR_VELOCITY_CALC;
  if (toEvent < fromEvent) then 
    toEvent = fromEvent; 
  end;

  local firstEvent = self.velocityCalculationInfo.touchEvents[fromEvent];
  local lastEvent = self.velocityCalculationInfo.touchEvents[toEvent];
  
  local dy = lastEvent.y - firstEvent.y
  local dt = lastEvent.time - firstEvent.time

  local dsY = math.sqrt(dy*dy)
  if dsY > 0 then
    dy = dy / dsY
  end

  if dt == 0 then 
    self.velocityCalculationInfo.velocity = 0;
  else
    self.velocityCalculationInfo.velocity = dsY/dt;
  end;

  if dy < 0 then self.velocityCalculationInfo.velocity = self.velocityCalculationInfo.velocity * -1; end;
  self.velocityCalculationInfo.dy = dy;
end;

Inertia.initInertia = function(self, touch) 
  if classInstanceContainer.instances[NODE.MOVE_STABILIZER.CLASS_NAME] and classInstanceContainer.instances[NODE.MOVE_STABILIZER.CLASS_NAME].inProgress then
    return;
  end;  

  self.bounceAreaEnteredVertical = nil;
  
  if not self:inertiaConditionsMet(touch) then return end;

  self.bounceAreaEnteredPos = nil;

  self.inertia.attenuationCalculated = false;
  self.inertia.attenuation = self.attr.attenuation;
  self.inertia.started = true;  
  self.inProgress = self.inertia.started;
end;

Inertia.inertiaConditionsMet = function(self, touch)
  local conditionsMet = self.inertiaAllowed 
  	    and self:targetIsMoving()
  	    and self:touchPointInsideBoundaries(touch)
  	    and self:velocityHighEnough()
  	    and self:targetInsideBoundaries();
  return conditionsMet;
end;

Inertia.targetIsMoving = function(self)
  return #self.velocityCalculationInfo.touchEvents > 1;
end;

Inertia.targetInsideBoundaries = function(self)
  local targetPos = self:getTargetPos()[POS_Y];
  return self:insideActiveBoundaries(targetPos);
end;

Inertia.touchPointInsideBoundaries = function(self, touch)
  local newPos = self:convertTouchToTargetPos(touch)[POS_Y];
  return self:insideActiveBoundaries(newPos);
end;

Inertia.velocityHighEnough = function(self)
  local velocityY = math.abs(self:getInertiaVelocity());
  local velocity = velocityY
  
  return velocity >= self.attr.minVelocity;
end;

Inertia.insideActiveBoundaries = function(self, pos)
  local insideY = true;
  insideY =  self:insideBoundaries(pos);
  return insideY;
end;


Inertia.insideBoundaries = function(self, pos)
  local inside = false;
  inside = pos > self.minPos and pos < self.maxPos;  	
  return inside;
end;

Inertia.getInertiaVelocity = function(self)
  local v = self:getVelocity();
  if self:belowMinVelocity(v) then
    v = self:getMinVelocity(v);
  end;
  return v;
end;

Inertia.belowMinVelocity = function(self, velocity)
  return math.abs(velocity) < self.inertia.MIN_VELOCITY;
end;

Inertia.getMinVelocity = function(self, velocity)
  if velocity < 0 then 
    velocity = self.inertia.MIN_VELOCITY * -1;
  else
    velocity = self.inertia.MIN_VELOCITY;
  end;  
  return velocity;
end;


Inertia.processInertia = function(self)
  if self.inertia.started then  
    
    if self.abortInertia then 
      self:stopInertia(); 
      return; 
    end;
    
    self.inertia.velocity = self:getInertiaVelocity();

    if self:velocityBelowMinVelocity() then   
      self:stopInertia();
      self:processPossibleBounceBack();
      return;
    end;
 
    local targetCurrentPos = self:getTargetPos()[POS_Y];
    
    if self:snapAllowed()then 
      self:pauseInertia();     
      self:calculateAttenuation(targetCurrentPos)
      self:resumeInertia();
    end;
    
    
    local newPos  = self:getNewVelocityAffectedPos();
    if self.bounceAreaEnteredVertical then
        self:processBounce(newPos);
    elseif self:bounceAreaEntered(newPos) then
        if self.bounceAreaEnteredPos == nil then self.bounceAreaEnteredPos = newPos; end;
        self.bounceAreaEnteredVertical = true;
        self:processBounce(newPos);
    else      
      Scene.set(self.target, 'Position', {'', newPos});
    end;    
    self:recalcVelocity();
  end;
end;

Inertia.bounceAreaEntered = function(self, pos)
  return not self:insideBoundaries(pos);
end;

Inertia.adjustPosToBoundaries = function(self, pos)
  local y = math.min(self.maxPos, pos);
  y = math.max(self.minPos, y);
  return y;  
end;

Inertia.processBounce = function(self, pos)
  if self:outsideBounceBoundaries(pos) then
    self:stopInertia();
    self:bounceBack(pos);          
  else
    Scene.set(self.target, 'Position', {'',pos});
  end;
end;

Inertia.getBounceDistance = function(self)
  local velocityFactor = math.abs(self:getInertiaVelocity()*100);
  local distance = self.attr.bounceDistance * velocityFactor;
  return distance;
end;


Inertia.bounceBack = function(self, startPos)
  local endPos = self:getTargetPos()[POS_Y];
  
  
  if startPos <= self.minPos then 
    endPos = self.minPos;
  elseif startPos >= self.maxPos then 
    endPos = self.maxPos;
  end;
  
  if math.floor(startPos) ~= math.floor(endPos) then
    local attribute = 'Position.Y';

    local distance = startPos - self.bounceAreaEnteredPos;
      local totDistance = startPos - endPos;
      local factor = totDistance / distance;
      Scene.animate(self.target, getAnim(startPos, endPos, self.attr.bounceAnimDuration, attribute));
  end;
end;

Inertia.processPossibleBounceBack = function(self)
  local targetPos = self:getTargetPos()[POS_Y];
  if self:withinBounceBoundaries(targetPos) then 
    self:bounceBack(targetPos);
  end;
end;

Inertia.withinBounceBoundaries = function(self, pos)
  local within = false;
  local maxBounce = self.maxPos + self:getBounceDistance();
  local minBounce = self.minPos - self:getBounceDistance();
    
  within = (pos > self.maxPos and pos < maxBounce) or
            (pos < self.minPos and pos > minBounce);  	
  return within;
end;


Inertia.outsideBounceBoundaries = function(self, pos)
  local outside = false;
  local max = self.maxPos + self:getBounceDistance();
  local min = self.minPos - self:getBounceDistance();
    
  outside = pos < min or pos > max; 
  return outside;
end;

Inertia.calculateAttenuation = function(self, pos)  
  self.inertia.attenuation = self.attr.attenuation;
  local endPos = self:calculateEndPos(self.inertia.velocity, self.inertia.attenuation, pos);

  local calcEndPos = endPos;

  local snapPos = self:getNextSnapPointInVelocityDirection(self.objects.snap.snapPointList, endPos, self.inertia.velocity);
  while self:endPosDifferFromSnapPos(calcEndPos, snapPos) do
    self.inertia.attenuation = self.inertia.attenuation + self.inertia.ATTENUATION_CALC_INC_STEP; 
    if self.inertia.attenuation > 0.995 then 
      self.inertia.attenuation = 0.995;
      break;
    end; 
    calcEndPos = self:calculateEndPos(self.inertia.velocity, self.inertia.attenuation, pos);
  end;
  self.inertia.attenuationCalculated = true;
end;




Inertia.endPosDifferFromSnapPos = function(self, endPos, snapPos)
  local differFrom = false;
  if self.inertia.velocity < 0 then 
    differFrom = endPos > snapPos
  else
    differFrom = endPos < snapPos
  end;
  return differFrom;
end;

Inertia.calculateEndPos = function(self, velocity, attenuation, startPos)
  local axisLength = Scene.HEIGHT;
  local endPos = startPos;
  while math.abs(velocity) > self.inertia.MIN_VELOCITY do 
    endPos = endPos + velocity * axisLength;
    velocity = velocity * attenuation;
  end;
  return endPos;
end;

Inertia.getClosestSnapPoint = function(self, snapPointList, pos)
  local closest = nil;
  local snapPos1 = self:getNextLowerSnapPoint(snapPointList, pos);
  local snapPos2 = self:getNextHigherSnapPoint(snapPointList,pos);

  if snapPos1 == nil and snapPos2 == nil then
    closest = pos;
  elseif snapPos1 == nil then
    closest = snapPos2
  elseif snapPos2 == nil then
    closest = snapPos1;
  elseif snapPos1 ~= nil and snapPos2 ~= nil then
    local diff1 = math.abs(snapPos1 - pos);
    local diff2 = math.abs(snapPos2 - pos);
    if diff1 < diff2 then  
       closest = snapPos1
    else 
       closest =  snapPos2;  
    end;
  end;
  return closest;
end;

Inertia.getNextSnapPointInVelocityDirection = function(self, snapPointList, pos, velocity)
  local snapPos = 0;
  if velocity < 0 then 
    snapPos = self:getNextLowerSnapPoint(snapPointList, pos);
    if snapPos == nil then
      snapPos = self:getNextHigherSnapPoint(snapPointList, pos);
    end;
  else 
    snapPos = self:getNextHigherSnapPoint(snapPointList, pos)
    if snapPos == nil then
      snapPos = self:getNextLowerSnapPoint(snapPointList, pos);
    end;
  end;
  return snapPos;
end;


Inertia.getNextLowerSnapPoint = function(self, snapPointList, pos)
  local snapPos = nil;
  for i = #snapPointList, 1, -1   do 
    if tonumber(snapPointList[i]) <= tonumber(pos) then 
      snapPos = tonumber(snapPointList[i]);
      break;
    end;
  end;
  return snapPos; 
end;


Inertia.getNextHigherSnapPoint = function(self, snapPointList, pos)
  local snapPos = nil;
  
  for i = 1, #snapPointList do 
    if tonumber(snapPointList[i]) >= tonumber(pos) then 
      snapPos = tonumber(snapPointList[i]);
      break;
    end;
  end;  
  return snapPos;
end;



Inertia.pauseInertia = function(self)
  self.inertia.started = false;
  self.inProgress = self.inertia.started;
end;

Inertia.resumeInertia = function(self)
  self.inertia.started = true;
  self.inProgress = self.inertia.started;
end; 

Inertia.getVelocity = function(self)
  local v = 0;
  if self.velocityCalculationInfo.velocity ~= nil then
    v = self.velocityCalculationInfo.velocity / Scene.FPS
  end;
  return v;
end;


Inertia.velocityBelowMinVelocity = function(self)
  local highestVelocity = math.abs(self.inertia.velocity);
  return (math.abs(highestVelocity)-0.00001) < self.inertia.MIN_VELOCITY;
end;

Inertia.snapAllowed = function(self)
  return not self.inertia.attenuationCalculated and self.objects.snap;
end;


Inertia.getNewVelocityAffectedPos = function(self)
  local pos = self:getTargetPos()[POS_Y];
  pos = pos + (self.inertia.velocity * Scene.HEIGHT)
  return pos;
end;

Inertia.recalcVelocity = function(self)
  self.velocityCalculationInfo.velocity = self.velocityCalculationInfo.velocity * self.inertia.attenuation;
end;










--  SNAP-----------------------------------------------------
--  ---------------------------------------------------------
--  ---------------------------------------------------------



Rubberband = class(TouchComponent);
Rubberband.target = nil;
Rubberband.whoAreYou = 'Rubberband Component';

Rubberband.attr = {
  rubberbandStrength = 0.2,
  releaseAnimDuration = 20
}

Rubberband.active = true;

Rubberband.rubberband = {
  inUse = false, 
  state = RUBBERBANDING_NONE;
}

Rubberband.moveInfo = {
  started = false,
}

Rubberband.minPos = 0;
Rubberband.maxPos = 0;
Rubberband.inProgress = false;
Rubberband.strength = 1 - 0.2;

Rubberband.init = function(self)
end;


Rubberband.processOnEveryFrame = function(self)

end;

Rubberband.processTouchDown = function(self, touch)
  if (not self.target) or (not self.active) then return; end;
  if self.rubberband.inUse then
    self:releaseRubberband();
  end;
  self.moveInfo.started = false;
end;

Rubberband.processTouchMove = function(self, touch)
  if (not self.target) or (not self.active) then return; end;
  self:processMove(touch);
end;

Rubberband.processTouchUp = function(self, touch)
  if (not self.target) or (not self.active) then return; end;
  if self.rubberband.inUse then
    self:releaseRubberband();
  end;
  self.moveInfo.started = false;
end;

Rubberband.releaseRubberband = function(self)

  local endPosition = self.minPos;
  local startPosition = self:getTargetPos()[POS_Y];
  local attribute = 'Position.Y';

  if self.rubberband.state == RUBBERBANDING_BOTTOM_SIDE then
     endPosition = self.maxPos;
  end;

  local anim = getAnim(startPosition, endPosition, self.attr.releaseAnimDuration, attribute);
  Scene.animate(self.target, anim);

  self:resetRubberbandState()
end;


Rubberband.processMove = function(self, touch)
  if classInstanceContainer.instances[NODE.MOVE_STABILIZER.CLASS_NAME] and classInstanceContainer.instances[NODE.MOVE_STABILIZER.CLASS_NAME].inProgress then
    return;
  end;  

  local newPos = self:convertTouchToTargetPos(touch)[POS_Y];
  if self:targetOutsideBoundary(newPos) then
    self:updateRubberbandState(newPos);
    self:calculateAndSetNewRubberbandedPos(newPos);
  else
    self:resetRubberbandState();
  end;
end;

Rubberband.targetOutsideBoundary = function(self, newPos)
  local outsideMaxY = self:targetOutsideMaxY(newPos);
  local outsideMinY = self:targetOutsideMinY(newPos);
  return outsideMaxY or outsideMinY;
end;

Rubberband.updateRubberbandState = function(self, newPos)
  if self:targetOutsideMaxY(newPos) then self:setRubberbandState(RUBBERBANDING_BOTTOM_SIDE)
  elseif self:targetOutsideMinY(newPos) then self:setRubberbandState(RUBBERBANDING_TOP_SIDE)
  else 
    self:resetRubberbandState();
  end;
end;

Rubberband.calculateAndSetNewRubberbandedPos = function(self, pos)
  local diff = self:getDistanceBetweenNewPosAndTargetEndpos(pos);
  if self:newPosOnWrongSideOfTargetEndPoint(diff) then return; end;
  
  local newPos = self:getRubberbandAffectedNewPos(diff);
  self:setTargetPos(newPos);
end;

Rubberband.getRubberbandAffectedNewPos = function(self, diff)
  local oldPos = self:getTargetPos()[POS_Y];
  local newY = oldPos;

  local newDiff = (diff^(self.strength));
  if self.rubberband.state == RUBBERBANDING_TOP_SIDE then 
  	newY =  self.minPos - newDiff; 
  elseif self.rubberband.state == RUBBERBANDING_BOTTOM_SIDE then
  	newY = self.maxPos + newDiff;
  else
    printError( 'ERROR! Rubberband state unknown in getRubberbandAffectedNewPos. '  .. self.whoAreYou);
    -- TODO.. .return 
    -- something is wrong.. errorhandling? Exceptions? .. I just return 0 to not halt the application. .that will set the list in orig pos
    return {0,0};
  end;
  return newY;
end;

Rubberband.setRubberbandState = function(self, state)
  self.rubberband.inUse = true;
  self.rubberband.state = state;
  self.inProgress = true;
end;

Rubberband.resetRubberbandState = function(self)
  self.rubberband.inUse = false;
  self.rubberband.state = RUBBERBANDING_NONE;
  self.inProgress = false;
end;

Rubberband.targetOutsideMaxY = function(self, pos)
  return math.ceil(pos) >= math.ceil(self.maxPos);
end;

Rubberband.targetOutsideMinY = function(self, pos)
  return pos <= self.minPos;
end;


Rubberband.getDistanceBetweenNewPosAndTargetEndpos = function(self, newPos)
  local diff = 0;
  if self.rubberband.state == RUBBERBANDING_TOP_SIDE then 
    diff = self.minPos - newPos
  elseif self.rubberband.state == RUBBERBANDING_BOTTOM_SIDE then 
    diff = newPos - self.maxPos;
  end;
  return diff;
end;

Rubberband.newPosOnWrongSideOfTargetEndPoint = function(self, diff)
  return diff < 0;
end;




































Group = class();
Group.parent = nil;
Group.node = nil;
Group.name = '';
Group.height = 0;
Group.pos = 0
Group.pendingHeight = nil;
Group.animation = nil;

Group.commit = function(self)
  local diff = 0;

  pendingAttr = 'pendingHeight';
  attr = 'height';

  diff = self[attr] - self[pendingAttr];
  self[attr] = self[pendingAttr];
  self[pendingAttr] = nil;
  return diff;
end;
--==========================================================================
--                           GROUP ARRANGE
--============================================A==============================
PRIO_LEVEL_1 = 0;
PRIO_LEVEL_2 = 1;
PRIO_LEVEL_3 = 2;

STATE_NORMAL = 0;
STATE_ROW_CHANGING = 1;

GroupArrange = class(TouchComponent);
GroupArrange.whoAreYou = 'Group Arrange Class';

GroupArrange.attr = {
  prefix = '',
  spacing = 10,
  expandUp = false,
  expandCenter = false,
  expandDown = true,
  designMode = false,  
  getSizeFrom = '',
  offset,
  expandUp = false
}

GroupArrange.state = STATE_NORMAL;
GroupArrange.stateCount = 20;
GroupArrange.lastChangeRow = 0;
GroupArrange.prioLevel = PRIO_LEVEL_1;
GroupArrange.prioChangeCount = 15;
GroupArrange._prefix = '';
GroupArrange.disable = false;

GroupArrange.active = true;
GroupArrange.size = 0;
GroupArrange._updating = false;
GroupArrange.groups = {};
GroupArrange.positionList = {};
GroupArrange.onChangeListeners = {};

GroupArrange.target = Scene.node();
GroupArrange.init = function(self)
  self.whoAreYou = 'groupArrange instance';
  self:readAttributes();
end;

local count = 0;
GroupArrange.processOnEveryFrame = function(self)
  if self.disable then return; end;
  if self._updating then return; end;
  local status, err = pcall(self._processOnEveryFrame, self);  
  self._updating = false;
end;

prioCounter = 0;
prioLevelChangeCounter = 0;
stateChangeCounter = 0;

GroupArrange._processOnEveryFrame = function(self)
  if not self.active then return; end;

  if self.state == STATE_ROW_CHANGING then
    stateChangeCounter = stateChangeCounter + 1;
    if stateChangeCounter > self.stateCount then
      self.state = STATE_NORMAL;
    end;
  end;
  
   
  if self.attr.designMode then 
    self:readAttributes();
  end;

  local status = self:_check();
  if status ~= CHANGE_NONE then
    self:_updateChanges(status);
  end;
end;

GroupArrange._updateChanges = function(self, status)
  if status == CHANGE_REBUILD then
    self:_rebuild();
  elseif status == CHANGE_SIZE then
    self:_rearrange();
  end;     
  self:onChange();
end;


GroupArrange.readAttributes = function(self)
  function change(attribute)
    local changed = false;
    if self.attr[attribute:firstCharLower()] ~= Scene.get(self.selfNode, attribute) then
      changed = true;
      self.attr[attribute:firstCharLower()] = Scene.get(self.selfNode, attribute);
    end;
    return changed;
  end;
  --======================================
    
  if change('GetSizeFrom') then
    doRefresh = true;
  end;

  if change('Prefix') then
    doRefresh = true;
    self._prefix = self.attr.prefix;
  end;
  
  if doRefresh then
    self:_rearrange();
  end;  
end;


GroupArrange.registerOnChangeListener = function(self, newListener, owner)
  if not newListener then 
  end;
  for registeredListener, instance in pairs(self.onChangeListeners) do
    if newListener == registeredListener then
      return;
    end;
  end;
  self.onChangeListeners[newListener] = owner;
end;

GroupArrange._filter = function(self, nodes)
  local filteredNodes = {};
  
  for _, node in ipairs(nodes) do 
    if Scene.name(node):sub(1,#self.attr.prefix) == self.attr.prefix then
      table.insert(filteredNodes, node);
    end;
  end;
  return filteredNodes;
end;

GroupArrange._getGroupNodeCount = function(self)
  local count = 0;
  if self.attr.prefix then
    count = getChildrenOfTypeCount(self.target, 'Group', self.attr.prefix)
  else
    count = getChildrenOfTypeCount(self.target, 'Group', self.attr.prefix)
  end;
  return count;
end;


GroupArrange._getGroupNodes = function(self)
  local nodes = {};
  if self.attr.prefix ~= '' then
    nodes = getChildrenOfType(self.target, 'Group', self.attr.prefix)
  else
    nodes = getChildrenOfType(self.target, 'Group')
  end;
  if self._prefix == '' then 
    self._prefix = self:getPrefix(nodes)
  end;
  nodes = self:sortGroupList(nodes);
  return nodes;
end;

GroupArrange.getPrefix = function(self, nodes)
  local prefix = '';
  local name = Scene.name(nodes[1]);
  for i = 1, #name do
    local pre = name:sub(1,i);
    local same = true;
    for j = 2, #nodes do
      if Scene.name(nodes[j]):sub(1, i) ~= pre then
        same = false;
        break;
      end;
      if same then 
        prefix = pre;
      else
        break
      end;
    end;    
  end;
  return prefix;
end;

GroupArrange._check = function(self)
  self._updating = true;
  local change = CHANGE_NONE;

  if self.state == STATE_ROW_CHANGING then  
    local node = Scene.find(Scene.parent(self.target), self._prefix .. self.lastChangeRow + 1);
    if self.groups[self.lastChangeRow + 1].node ~= node then
      self.lastChangeRow = self.lastChangeRow + 1;
      Scene.set(node, 'Position', {'',self.groups[self.lastChangeRow].pos[POS_Y]});
      self.groups[self.lastChangeRow].node = node;
      if self.lastChangeRow >= #self.groups then
        self.state = STATE_NORMAL;
        return CHANGE_NONE;
      else
        self.state = STATE_ROW_CHANGING;
        stateChangeCounter = 0;
        return CHANGE_NONE;
      end;
    end;
    return CHANGE_NONE;
  end;
  
  local nodes = self:_getGroupNodes();

  function nodeCountHasChanged()
    local hasChanged = #nodes ~= #self.groups;
    return hasChanged;
  end;

  if #nodes == 0 then return; end;
  if nodeCountHasChanged() then
    self:_createInternalNodeList(nodes); -- if we init we automatically adjust the sizes
    change = CHANGE_REBUILD;
  else
    if not self:_compare(nodes, self.groups) then
      self:_synchInfo(nodes);
      change = CHANGE_SIZE;
    end
  end;
  return change;
end;


GroupArrange._compare = function(self, nodes, list)
  local isEqual = true;

  for index, node in pairs(nodes) do   
    local sizeNode = node;
    if self.attr.getSizeFrom ~= '' then
      sizeNode = Scene.find(node, self.attr.getSizeFrom);
    end;
    local size = getNodeSize(sizeNode);  
    local nameEqual = list[index].name == Scene.name(node);
    local nodeEqual = list[index].node == node;
    if not nodeEqual then
      list[index].node = node;
      self.lastChangeRow = index;
      Scene.set(node, 'Position', {'', list[index].pos[POS_Y]});
      self.state = STATE_ROW_CHANGING;
      stateChangeCounter = 0;
      nodeEqual = true;
    end;
    local heightEqual = list[index].height == size[HEIGHT];
    isEqual = nameEqual and
              nodeEqual and
              heightEqual
     if not isEqual then break;end;
  end;
  return isEqual;
end;

GroupArrange._synchInfo = function(self, nodes)
  for index, node in ipairs(nodes) do   
    local sizeNode = node;
    if self.attr.getSizeFrom ~= '' then
      sizeNode = Scene.find(node, self.attr.getSizeFrom);
    end;
    local size = getNodeSize(sizeNode);  
    local pos = Scene.get(node, 'Position')[POS_Y];
    
    if self.groups[index].height ~= size[HEIGHT] then
      self.groups[index].pendingHeight = size[HEIGHT];
    else
      self.groups[index].pendingHeight = nil;
    end;
    
   if self.groups[index].node ~= node then
      self.groups[index].node = node;
    end;
    
  end;  
end;


GroupArrange._createInternalNodeList = function(self, nodes)  
  if #nodes == 0 then return; end;
  self.groups = {};
  local defaultPos = Scene.get(nodes[1], 'Position')[POS_Y];
  for i = 1, #nodes do
     local sizeNode = nodes[i];
     if self.attr.getSizeFrom ~= '' then
       sizeNode = Scene.find(nodes[i], self.attr.getSizeFrom);
     end;
     local size = getNodeSize(sizeNode);  
     local pos = Scene.get(nodes[i], 'Position')[POS_Y];
     local group = Group:new();
     group.node = nodes[i];
     group.name = Scene.name(nodes[i]);
     group.height = size[HEIGHT];
     group.pos = pos;
     table.insert(self.groups, group);
  end;
end;

GroupArrange._refreshTotalSize = function(self)
  local totHeight = 0;
  
  for _, group in ipairs(self.groups) do
    totHeight = totHeight + group.height + self.attr.spacing;
  end;
  self.size = totHeight - self.attr.spacing;
end;

GroupArrange._updateNodePos = function(self, node, pos)
  node.pos = pos
  local rowNode = Scene.find(Scene.parent(self.target), node.name);
  
  Scene.set(rowNode, 'Position', {'', pos});
end;

GroupArrange._getExpandFactor = function(self)
  local expandFactor = 0;
  if self.attr.expandCenter then
    if self.attr.horizontal then
      expandFactor = -0.5
    else
      expandFactor = 0.5
    end;
  elseif self.attr.expandUp then
    if self.attr.horizontal then
      expandFactor = -1;
    else
      expandFactor = 1;
    end;
  end;
  return expandFactor;
end;

GroupArrange._rebuild = function(self)
  if #self.groups == 0 then return; end;

  local curPos = 0;
  local prevNode = nil;
  for index, node in ipairs(self.groups) do       
    curPos = self:_getNextPosition(curPos, node, prevNode);

    newPos = curPos;
    self:_updateNodePos(node, newPos);
    prevNode = node;
  end;
  self:_refreshTotalSize();  
end;

GroupArrange._rearrange = function(self)
  if #self.groups == 0 then return; end;

  local newPos = 0;
  local curPos = 0;
  local prevNode = nil;
  for index, node in ipairs(self.groups) do       
    if self:_nodeHasChangedSize(node) then
      local diff = node:commit();      
      if diff ~= 0 then        
        self:_adjustGroupParentAccordingToExpandType(diff);      
      end;
    end;

    curPos = self:_getNextPosition(curPos, node, prevNode);

    newPos = curPos;

    self:_updateNodePos(node, newPos);
    prevNode = node;
  end;
  self:_refreshTotalSize();  
end;


GroupArrange._nodeHasChangedSize = function(self, node) 
  return node.pendingHeight;
end;

GroupArrange._adjustGroupParentAccordingToExpandType = function(self, deltaPos)
  local expandFactor = self:_getExpandFactor();
  deltaPos  = deltaPos * expandFactor;
  local pos = Scene.get(self.target, 'Position')[POS_Y]; 
  pos = pos - deltaPos;      
  if expandFactor == 1 and pos < 0 then
    pos = 0;
  end;
    Scene.set(self.target, 'Position', {'',pos});
end;

GroupArrange._getNextPosition = function(self, curPos, node, prevNode)
  local newPos = nil;
  if prevNode then
    newPos = curPos - (prevNode.height/2) - (node.height/2) - self.attr.spacing;
  else
    newPos = curPos - (node.height/2);
  end;
  return newPos
end;

GroupArrange.onChange = function(self)
  for listener, owner in pairs(self.onChangeListeners) do 
    pcall(listener, owner);
  end;
end;

GroupArrange.getGroupPositions = function(self)
  local posList = {};
  local cnt = 0;
  for _, group in ipairs(self.groups) do
	table.insert(posList, cnt);
    cnt = cnt + group.height + self.attr.spacing;
    
  end;
  return posList;  
end;

GroupArrange.sortGroupList = function(self, groupList)
-- using bubblesort because the list is almost always already sorted
  function compareNames(name1, name2)
     local suff1 = name1:sub(#self._prefix + 1); 
     local suff2 = name2:sub(#self._prefix + 1);
     local num1 = name1:sub(#self._prefix + 1); 
     local num2 = name2:sub(#self._prefix + 1);
          
     local num1 = tonumber(suff1);
     local num2 = tonumber(suff2);
     local n1 = name1:sub(1, #name1 - #suff1);
     local n2 = name2:sub(1, #name2 - #suff2);
     
     if n1 > n2 then
       return true;
     elseif n1 < n2 then
       return false;
     else
       if not num1 then 
         return false
       elseif not num2 then
         return true
       else
         return (num1 > num2);       
       end;
     end;
  end;
  
  repeat     
    local swapped = false
    for i = 2,  #groupList do
       if compareNames(Scene.name(groupList[i-1]), Scene.name(groupList[i])) then
         groupList[i], groupList[i-1] = groupList[i-1], groupList[i];
         swapped = true;
         break;
       end
     end
   until not swapped
   return groupList;
end;
































 








ScrollerBoundary = class(TouchComponent);
ScrollerBoundary.whoAreYou = 'ScrollerBoundary Class';

ScrollerBoundary.attr = {
  topLowest = 0,
  bottomHighest = 0,
  nodeHeight = 0,
  calculatedMinPos = 0,
  calculatedMaxPos = 0
}


ScrollerBoundary.objects.groupArrange = nil;

ScrollerBoundary.onChangeListeners = {};

ScrollerBoundary.init = function(self)
  self.whoAreYou = 'scrollerboundary instance';  
  self.objects.groupArrange = classInstanceContainer.instances[NODE.GROUP_ARRANGE.CLASS_NAME];

  self.objects.groupArrange:registerOnChangeListener(self.onGroupArrangeChange, self);
  self:readAttributes();
end;

ScrollerBoundary.readAttributes = function(self)
  function change(attribute)
    local changed = false;
    if self.attr[attribute:firstCharLower()] ~= Scene.get(self.selfNode, attribute) then
      changed = true;
      self.attr[attribute:firstCharLower()] = Scene.get(self.selfNode, attribute);
    end;
    return changed;
  end;
  --======================================
--  local refresh = change('TopLowest') or
--  			   change('BottomHighest') or
--  			   change('LeftHighest') or
--  			   change('RightLowest') or
--                  change('NodeSize') or
--                  change('ScreenCoordinates');
--  if refresh then 
--    self:calculate();
--  end;
end;

ScrollerBoundary.calculate = function(self)
 
  self.objects.groupArrange:_rebuild();
  totalHeight = self.objects.groupArrange.size;

  self.attr.calculatedMinPos = self.attr.topLowest;
  self.attr.calculatedMaxPos = self.attr.bottomHighest - self.attr.topLowest + totalHeight;
  self.attr.calculatedMaxPos = math.max(self.attr.calculatedMinPos, self.attr.calculatedMaxPos);
  self:onChange();
end;

ScrollerBoundary.onChange = function(self)
  for listener, owner in pairs(self.onChangeListeners) do 
    pcall(listener, owner);
  end;
end;

ScrollerBoundary.onGroupArrangeChange = function(self)
    self:calculate();
end;

ScrollerBoundary.registerOnChangeListener = function(self, newListener, owner)
  if not newListener then 
  end;
  for registeredListener, instance in pairs(self.onChangeListeners) do
    if newListener == registeredListener then
      return;
    end;
  end;
  self.onChangeListeners[newListener] = owner;
end;











































--==========================================================================
--                          SNAP CLASS
--============================================A==============================
Snap = class(TouchComponent);
Snap.whoAreYou = 'Snap Class';

Snap.attr = {
  toClosest = false,
  toClosestHigher = false,
  toClosestLower = false,
  offset = {0,0,0},
  drawGrid = false,
  everyNth = 1,
}

Snap.minPos = 0;
Snap.maxPos = 0;

Snap.snapPointList = {};

Snap.groupArrangeSnapPointList = {};

Snap.objects.inertia = nil;
Snap.objects.rubberband = nil;
Snap.objects.groupArrange = nil;

Snap.allowed = true;
Snap.snapMethod = SNAP_TO_CLOSEST;

Snap.drawTool = nil;

Snap.processOnEveryFrame = function(self)
  if self.drawGrid then
    self:readAttributes();
  end;
end;

Snap.init = function(self)
  self.whoAreYou = 'snap instance';  
  if classInstanceContainer then
    self.objects.inertia = classInstanceContainer.instances[NODE.INERTIA.CLASS_NAME];
    self.objects.groupArrange = classInstanceContainer.instances[NODE.GROUP_ARRANGE.CLASS_NAME];
    self.objects.rubberband = classInstanceContainer.instances[NODE.RUBBERBAND_CLASS_NAME];
  end;

  if self.objects.groupArrange then
    self.objects.groupArrange:registerOnChangeListener(self.onGroupArrangeChange, self);
  end;
  self:readAttributes();
end;

Snap.readAttributes = function(self)
  function change(attribute)
    local changed = false;
    if self.attr[attribute:firstCharLower()] ~= Scene.get(self.selfNode, attribute) then
      changed = true;
      self.attr[attribute:firstCharLower()] = Scene.get(self.selfNode, attribute);
    end;
    return changed;
  end;
  --======================================

  change('Offset');
  change('EveryNth')
  change('ToClosest');
  change('ToClosestHigher');
  change('ToClosestLower');
  

  self:populateSnapLists();
end;

Snap.onGroupArrangeChange = function(self)
    self:populateSnapLists();
end;


Snap._setDrawingTool = function(self)
  if self.drawTool then Scene.set(self.drawTool, 'Data', ''); end;
  self.drawTool = getFreehandStroke(self.selfNode, self:getAttribute('FreehandStroke'), true); 
  if self.drawTool then
    Scene.set(self.drawTool, 'EndLength', 0);
    Scene.set(self.drawTool, 'Filter', false);
    Scene.set(self.drawTool, 'Thickness', 2);  
    Scene.set(self.drawTool, 'Color', {0.5, 0.4, 0.2});
  end;
end;

Snap.processTouchDown = function(self, touch)
  if not self.target then return; end;
end;

Snap.processTouchMove = function(self, touch)
  if not self.target then return; end;
end;


Snap.processTouchUp = function(self, touch)
  --if self.objects.inertia and self.objects.inertia.inProgress then return; end;
  if classInstanceContainer.instances[NODE.INERTIA.CLASS_NAME] and classInstanceContainer.instances[NODE.INERTIA.CLASS_NAME].inProgress then
    return;
  end;  
  if classInstanceContainer.instances[NODE.MOVE_STABILIZER.CLASS_NAME] and classInstanceContainer.instances[NODE.MOVE_STABILIZER.CLASS_NAME].inProgress then
    return;
  end;  
  if classInstanceContainer.instances[NODE.RUBBERBAND.CLASS_NAME] and classInstanceContainer.instances[NODE.RUBBERBAND.CLASS_NAME].inProgress then
    return;
  end;  


  --TODO connect to inertia/rubberband groupArrange to see if we should take values from them
  self:initSnapMethod();
  -- TODO check inertia and rubberbanding and such here and return if they are working..
  local convertedPos = self:convertTouchToTargetPos(touch)[POS_Y];
--  if not self.attr.horizontal then 
--    convertedPos[POS_X] = Scene.get(self.target, 'Position')[POS_X];
--  end;
--  if not self.attr.vertical then 
--    convertedPos[POS_Y] = Scene.get(self.target, 'Position')[POS_Y];
--  end;
  status = self:_outsideBoundaries(convertedPos);
  self:_processSnap(convertedPos, status);
end;

Snap._outsideBoundaries = function(self, pos)
  function checkBoundaries(self, pos)
    local status = SNAP_VALID;
    if pos < self.minPos then
      status = SNAP_OUTSIDE_MIN;
    elseif pos > self.maxPos then
      status = SNAP_OUTSIDE_MAX;
    end
    return status;
  end;
  --=====================================
  local status = false;
  status = checkBoundaries(self, pos);

  return status;
end;

Snap.initSnapMethod = function(self)
  local method = SNAP_TO_NONE;
  if self.attr.toClosest then 
    method = SNAP_TO_CLOSEST;
  elseif self.attr.toClosestHigher then
    method = SNAP_TO_CLOSEST_HIGHER;
  elseif self.attr.toClosestLower then
    method = SNAP_TO_CLOSEST_LOWER;
  end;

  self.snapMethod = method;
end;

Snap._processSnap = function(self, pos, status)
  self:_adjustMethodByStatus(status);
  if self.snapMethod ~= SNAP_TO_NONE then
    self:_doSnap(pos);
  end;
end;

Snap._doSnap = function(self, pos)
  function getSnapPoint(self, default)
    local snapPoint = default;
    if self.snapMethod == SNAP_TO_CLOSEST then
      snapPoint = self:_getClosestPoint(pos);
    elseif self.snapMethod == SNAP_TO_CLOSEST_HIGHER then
      snapPoint = self:_getClosestHigherPoint(pos);
    elseif self.snapMethod == SNAP_TO_CLOSEST_LOWER then
      snapPoint = self:_getClosestLowerPoint(pos);
    end;
    return snapPoint;
  end;
  --============================================  
  local to = 0;
  to = getSnapPoint(self, pos);
  local duration = 20;  

  Scene.animate(self.target, getAnim(pos, to, duration, 'Position.Y'));
end;

Snap._adjustMethodByStatus = function(self, status)
  function adjust(status, default)
    local method = default;
    if status == SNAP_OUTSIDE_MAX then
      method = SNAP_TO_CLOSEST_LOWER;
    elseif status == SNAP_OUTSIDE_MIN then
      method = SNAP_TO_CLOSEST_HIGHER;
    end;
    return method;
  end;
  --====================================
  self.snapMethod = adjust(status, self.snapMethod);
end;

Snap.populateSnapLists = function(self)
  if self.objects.groupArrange then
    self:getGroupArrangeList();
    self:copyGroupArrangeListToSnapPointList();
    self:_drawSnapGrid(self.attr.drawGrid);
    return;
  end;
  
  self:populateSnapPointList();  
end;

Snap.getGroupArrangeList = function(self)
  self.groupArrangeSnapPointList = self.objects.groupArrange:getGroupPositions();
end;

Snap.copyGroupArrangeListToSnapPointList = function(self)
  self.snapPointList = {};
  for ix, value in pairs(self.groupArrangeSnapPointList) do
    if math.mod(ix-1, self.attr.everyNth) == 0 then
      table.insert(self.snapPointList, value + self.attr.offset);
    end;
  end;
end;

Snap.populateSnapPointList = function(self, axis)  
    self.snapPointList = {};

    local lowestSnapPoint = self.minPos + self.attr.offset;
    local highestSnapPoint = self.maxPos + self.attr.snapInterval + self.attr.offset; 

    local snapPoint = lowestSnapPoint;
    while snapPoint <  highestSnapPoint do
      table.insert(self.snapPointList, snapPoint);
      snapPoint = snapPoint + self.attr.snapInterval;
    end;
end;

Snap._getClosestPoint = function(self, pos)
  if (not self.snapPointList) or (#self.snapPointList == 0) then
    error('ERROR Failed to find snapPoint. SnapPointList is empty or nil', 2);
  end;

  local low = EXTREMELY_LOW;
  local high = EXTREMELY_HIGH;
  for i = 1, #self.snapPointList do
    if self.snapPointList[i] < pos then
      low = self.snapPointList[i];
    else
      high = self.snapPointList[i];
      break;
    end;
  end;

  if math.abs(pos - low) < math.abs(pos - high) then
    return low
  else
    return high
  end;
end;


Snap._getClosestHigherPoint = function(self, axis, pos)
  if (not self.snapPointList) or (#self.snapPointList == 0) then
    error('ERROR Failed to find snapPoint. SnapPointList is empty or nil', 2);
  end;
  
  local closest = EXTREMELY_LOW;
  for i = 1, #self.snapPointList do
    if self.snapPointList[i] > pos then
      closest = self.snapPointList[i];
      break;
    end;
  end;
  if closest == EXTREMELY_LOW then 
    closest = self:_getClosestLowerPoint(pos);
  end;
  return closest;
end;

Snap._getClosestLowerPoint = function(self, pos)
  if (not self.snapPointList) or (#self.snapPointList == 0) then
    error('ERROR Failed to find snapPoint. SnapPointList is empty or nil', 2);
  end;
  local closest = EXTREMELY_LOW;
  for i = 1, #self.snapPointList do
    if self.snapPointList[i] < pos then
      closest = self.snapPointList[i];
    else
      break;
    end;
  end;
  if closest == EXTREMELY_LOW then
    closest = self:_getClosestHigherPoint(pos);
  end;
  
  return closest;
end;

Snap._drawSnapGrid = function(self, draw)

  function getLowPoints()
    -- get the lowest snapPoints OR if we ar not snapping in a direction.. setLowestPoint on screen to be able to draw.. 
    local low = self.snapPointList[1];
    return low;
  end;
  function getHighPoints()
    -- get the highest snapPoints OR if we ar not snapping in a direction.. setHighistPoint on screen to be able to draw.. 
    local high = self.snapPointList[#self.snapPointList];
    return high;
  end;

  --================================================
  
  if not self.drawTool then return; end;
  
  Scene.set(self.drawTool, 'Data', '');  
  if not draw then return; end;
  
  local pipe = '|';
  local comma = ',';
  local data = '';

  local low = getLowPoints()
  local high = getHighPoints();


  for _, value in ipairs(self.snapPointList) do
      data = data ..high ..comma..value .. pipe..low..comma.. value ..pipe.. high ..comma..value ..pipe; 
    end;
  Scene.set(self.drawTool, 'Data', data);
end;









--==========================================================================
--                           MOVESTABILIZER
--==========================================================================
MoveStabilizer = class(TouchComponent);
MoveStabilizer.target = Scene.node();
MoveStabilizer.whoAreYou = 'MoveStabilzier Component';

MoveStabilizer.attr = {
  distance = 10,
  catchUp = true,
  catchUpFactor = 1.2,
  minPos = 0,
  maxPos = 0,
}

MoveStabilizer.inProgress = false;
MoveStabilizer._stabilizeInProgress = false;
MoveStabilizer._distanceExceeded = false;
MoveStabilizer._catchUpDone = false;
MoveStabilizer._touchList = {};
MoveStabilizer.catchedUp = true


MoveStabilizer.init = function(self)
  self.whoAreYou = 'moveStabilizer instance';
end;



MoveStabilizer.processOnEveryFrame = function(self)

end;

MoveStabilizer.processTouchDown = function(self, touch)
  self._touchList = {};
  self:addTouch(touch);
  self:startStabilize();
end;

MoveStabilizer.processTouchMove = function(self, touch)
  self:addTouch(touch);
  self:processStabilize(touch);
end;


MoveStabilizer.processTouchUp = function(self, touch)
end;

MoveStabilizer.addTouch = function(self, touch)
  self._touchList[STABILIZER_PREVIOUS_TOUCH] = self._touchList[STABILIZER_CURRENT_TOUCH]
  self._touchList[STABILIZER_CURRENT_TOUCH] = touch;
end;

MoveStabilizer.startStabilize = function(self)
  self._distanceExceeded = false;
  self._catchUpDone = false;
  self._stabilizeInProgress = true;
  self.inProgress = true;
end;

MoveStabilizer.processStabilize = function(self, touch)
  if not self._stabilizeInProgress then return; end;

  if self:_outsideBoundaries(touch) then
    self:endStabilize();
    return;
  end;

  if self._distanceExceeded then
    if (not self.attr.catchUp) or self._catchUpDone then
      self:endStabilize();      
    else
      self:processCatchUp(touch);
    end;
  elseif self:moveExceedMinDistance(touch) then
    self._distanceExceeded = true;
    self.inProgress = false;
 
    if self.attr.catchUp then 
      self:initCatchUp(touch);
    else
      self._catchUpDone = true;
    end;
  end;
end;

MoveStabilizer.initCatchUp = function(self, touch)
  self.catchedUp = false;
  self._catchUpDone = false;
end;

MoveStabilizer.endStabilize = function(self)
  self._stabilizeInProgress = false;
  self.inProgress = false;
end;


MoveStabilizer.processCatchUp = function(self, touch)
  if not self.attr.catchUp then
    self._catchUpDone = true;
    return;
  else
    self:doCatchUp(touch);    
  end;  
end;

MoveStabilizer.moveExceedMinDistance = function(self, touch)
  return self:moveExceedMinDistanceVertical(touch)
end;


MoveStabilizer.moveExceedMinDistanceVertical = function(self, touch)
  local movedDistanceY = 0;
  movedDistanceY = math.abs(touch.y - self.touchInfo.touchDown.y) * Scene.HEIGHT;
  return tonumber(movedDistanceY) > tonumber(self.attr.distance);
end;


MoveStabilizer.doCatchUp = function(self, touch)
  local curTouchPos = convertTouchToPos(touch)[POS_Y];
  local lastTouchPos = convertTouchToPos(self._touchList[STABILIZER_PREVIOUS_TOUCH])[POS_Y];
  local targetPos = self:getTargetPos()[POS_Y];
  local wantedPos = curTouchPos - self.touchInfo.offsetBetweenTouchYAndTargetPosY
  
  local touchDelta = curTouchPos - lastTouchPos;
  
  local newPos = 0;

  function _getShortenedDistanceBetweenWantedPosAndActualPos()
    local newDistance =  touchDelta*self.attr.catchUpFactor;
    if (curTouchPos < lastTouchPos) and (targetPos < wantedPos) then
      newDistance = touchDelta/self.attr.catchUpFactor;
    end;
    return newDistance;
  end;

  function _weOvershotWantedPos()
    return ((targetPos < wantedPos) and (newPos < targetPos)) or
           ((targetPos > wantedPos) and (newPos > targetPos))
  end;

  function _getNewPos()
    local newCatchedUpDistance = 0;
    newCatchedUpDistance = _getShortenedDistanceBetweenWantedPosAndActualPos();
    return targetPos + newCatchedUpDistance;
  end;

  function _adjustPosIfWeOvershot()
    if _weOvershotWantedPos() then
      newPos = wantedPos;
      self.catchedUp = true; 
    end; 
  end;
  --========================================

  newPos = _getNewPos();
  _adjustPosIfWeOvershot();


  local updatedPos = targetPos;

  updatedPos = newPos;  

  self:setTargetPos(updatedPos);
  self._catchUpDone = self.catchedUp;  
end;

MoveStabilizer._outsideBoundaries = function(self, touch)
  local pos = self:convertTouchToTargetPos(touch)[POS_Y];
  local outsideVertical = pos < self.attr.minPos or pos > self.attr.maxPos;
  return outsideVertical;
end;


