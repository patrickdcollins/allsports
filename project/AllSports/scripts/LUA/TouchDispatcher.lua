TouchDispatcher = {}

function TouchDispatcher:new()
	object = {}
	setmetatable(object, self)
	self.__index = self

	object._pickTag = nil

	return object
end

-- transform screen normalized coordinate [Sx,Sy]
-- to local node coordinate
-- returns nil when conversion cannot be made
function TouchDispatcher:convScreenToLocal2D(mi, Sx, Sy)
  local Sz = -(mi[9]*Sx + mi[10]*Sy + mi[12]) / mi[11];
  local Lx = mi[1] *Sx + mi[2] *Sy + mi[3] *Sz + mi[4];
  local Ly = mi[5] *Sx + mi[6] *Sy + mi[7] *Sz + mi[8];
  local Lw = mi[13]*Sx + mi[14]*Sy + mi[15]*Sz + mi[16];
  if Lw < 0 then
    return nil
  end
    return {Lx/Lw, Ly/Lw};
end

function TouchDispatcher:shallowcopy(orig)
  local orig_type = type(orig)
  local copy
  if orig_type == 'table' then
    copy = {}
    for orig_key, orig_value in pairs(orig) do
      copy[orig_key] = orig_value
    end
  else -- number, string, boolean, etc
    copy = orig
  end
  return copy
end

function TouchDispatcher:onTouchPrimary(touches)
  for i=1,#touches do
    local touch = touches[i]
    if touch.primary then
      if touch.type == Events.DOWN then
        local picks = touch.pick
        if picks ~= nil then
          for j=1,#picks do
            local hit = touch.pick[j]
            if hit.top then
              for k=1,#hit.tags do
                local tag = hit.tags[k]
                if self._pickTag == nil or self._pickTag == tag then
                  touch.data.pushedTouchID = touch.id
                  touch.data.time = touch.time
                  touch.data.node = hit.node
                  touch.data.invmat = hit.invmat
                  touch.data.initialPos = self:convScreenToLocal2D(hit.invmat, touch.x, touch.y)
                  self:mousePressed(touch)
                end
              end
            end
          end
        end
      else
        if touch.data == nil then
          -- how this can happen?
          return
        end
        if touch.data.pushedTouchID == touch.id then
          if touch.type == Events.UP then
            touch.data.pushedTouchID = nil
            self:mouseReleased(touch)
          else
            self:mouseDragged(touch)
          end
        end
      end
    end
  end
end
