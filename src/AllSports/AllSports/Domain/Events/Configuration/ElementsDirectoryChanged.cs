﻿namespace AllSports.Domain.Events.Configuration
{
    using Infrastructure.Messaging;

    public class ElementsDirectoryChanged : IDomainEvent
    {
        public ElementsDirectoryChanged(string directory)
        {
            this.ElementsDirectory = directory;
        }

        public string ElementsDirectory { get; }
    }
}