﻿namespace AllSports.Domain.Events.Configuration
{
    using Infrastructure.Messaging;

    public class GS2HostChanged : IDomainEvent
    {
        public GS2HostChanged(string gs2Host)
        {
            this.GS2Host = gs2Host;
        }

        public string GS2Host { get; }
    }
}