﻿namespace AllSports.Domain.Events.Configuration
{
    using Infrastructure.Messaging;

    public class GS2ProjectChanged : IDomainEvent
    {
        public GS2ProjectChanged(string gs2Project)
        {
            this.GS2Project = gs2Project;
        }

        public string GS2Project { get; }
    }
}