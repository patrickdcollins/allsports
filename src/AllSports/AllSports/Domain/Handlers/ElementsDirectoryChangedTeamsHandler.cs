﻿namespace AllSports.Domain.Handlers
{
    using Events.Configuration;
    using Infrastructure.Messaging;
    using Model;

    public class ElementsDirectoryChangedTeamsHandler : IHandleMessages<ElementsDirectoryChanged>
    {
        private readonly Teams _teams;

        public ElementsDirectoryChangedTeamsHandler(Teams teams)
        {
            this._teams = teams;
        }

        public IBusMessages Bus { get; set; }

        public void Handle(ElementsDirectoryChanged message)
        {
            this._teams.UpdateOptions(message.ElementsDirectory);
        }
    }
}