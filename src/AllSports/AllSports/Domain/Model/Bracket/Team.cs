﻿namespace AllSports.Domain.Model.Bracket
{
    public class Team
    {
        public string Directory { get; set; } = string.Empty;

        public string Name { get; set; } = string.Empty;

        public string ID { get; set; } = string.Empty;

        public string Rank { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public bool IsValid =>
            !string.IsNullOrEmpty(this.Directory);

        public string LogoPath =>
            this.IsValid
                ? $"{this.Directory}\\ELEMENTS\\ELEMENT 033_LOGO CHIP.png"
                : ".\\Resources\\black.png";
    }
}