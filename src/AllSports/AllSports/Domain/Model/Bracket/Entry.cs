﻿namespace AllSports.Domain.Model.Bracket
{
    public class Entry
    {
        public Team Top { get; set; } = new Team();

        public Team Bottom { get; set; } = new Team();

        public string Description { get; set; } = string.Empty;

        public string TopScore { get; set; } = string.Empty;

        public string BottomScore { get; set; } = string.Empty;
    }
}
