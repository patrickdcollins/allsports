﻿namespace AllSports.Domain.Model.Bracket
{
    using System.Linq;
    using Newtonsoft.Json;

    public class Bracket
    {
        private string _selectedSport;

        public Bracket()
        {
            this.SelectedSport = string.Empty;
            this.FirstLevelEntries = Enumerable.Range(1, 32).Select(i => new Entry()).ToArray();
            this.SecondLevelEntries = Enumerable.Range(1, 16).Select(i => new Entry()).ToArray();
            this.ThirdLevelEntries = Enumerable.Range(1, 8).Select(i => new Entry()).ToArray();
            this.FourthLevelEntries = Enumerable.Range(1, 4).Select(i => new Entry()).ToArray();
            this.FifthLevelEntries = Enumerable.Range(1, 2).Select(i => new Entry()).ToArray();
            this.SixthLevelEntry = new Entry();

            this.Title = "Bracket";
            this.Group1 = "WEST";
            this.Group2 = "SOUTH";
            this.Group3 = "EAST";
            this.Group4 = "MIDWEST";

            this.SponsorFilePath = string.Empty;
            this.Menu1 = "ROUND 2";
            this.Menu2 = "ROUND 3";
            this.Menu3 = "SWEET 16";
            this.Menu4 = "ELITE 8";
            this.Menu5 = "FINAL 4";
            this.Menu6 = "CHAMPION";
        }

        public string Title { get; set; }

        public string Group1 { get; set; }

        public string Group2 { get; set; }

        public string Group3 { get; set; }

        public string Group4 { get; set; }

        public string SponsorFilePath { get; set; }

        public string Menu1 { get; set; }

        public string Menu2 { get; set; }

        public string Menu3 { get; set; }

        public string Menu4 { get; set; }

        public string Menu5 { get; set; }

        public string Menu6 { get; set; }

        public string SelectedSport
        {
            get { return this._selectedSport; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this._selectedSport = value;
                }
            }
        }

        public Entry[] FirstLevelEntries { get; set; }

        public Entry[] SecondLevelEntries { get; set; }

        public Entry[] ThirdLevelEntries { get; set; }

        public Entry[] FourthLevelEntries { get; set; }

        public Entry[] FifthLevelEntries { get; set; }

        public Entry SixthLevelEntry { get; set; }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public void Deserialize(string json, Teams teams)
        {
            try
            {
                var bracket = JsonConvert.DeserializeObject<Bracket>(json);

                this.SelectedSport = bracket.SelectedSport;

                this.Title = bracket.Title;
                this.Group1 = bracket.Group1;
                this.Group2 = bracket.Group2;
                this.Group3 = bracket.Group3;
                this.Group4 = bracket.Group4;

                this.SponsorFilePath = bracket.SponsorFilePath;
                this.Menu1 = bracket.Menu1;
                this.Menu2 = bracket.Menu2;
                this.Menu3 = bracket.Menu3;
                this.Menu4 = bracket.Menu4;
                this.Menu5 = bracket.Menu5;
                this.Menu6 = bracket.Menu6;

                for (var i = 0; i < this.FirstLevelEntries.Length; i++)
                {
                    if (!string.IsNullOrEmpty(bracket.FirstLevelEntries[i].Top.ID))
                        this.FirstLevelEntries[i].Top = teams.HashedTeams[bracket.SelectedSport][bracket.FirstLevelEntries[i].Top.ID];

                    if (!string.IsNullOrEmpty(bracket.FirstLevelEntries[i].Bottom.ID))
                        this.FirstLevelEntries[i].Bottom = teams.HashedTeams[bracket.SelectedSport][bracket.FirstLevelEntries[i].Bottom.ID];

                    this.FirstLevelEntries[i].Description = bracket.FirstLevelEntries[i].Description;
                    this.FirstLevelEntries[i].TopScore = bracket.FirstLevelEntries[i].TopScore;
                    this.FirstLevelEntries[i].BottomScore = bracket.FirstLevelEntries[i].BottomScore;
                }

                for (var i = 0; i < this.SecondLevelEntries.Length; i++)
                {
                    if (!string.IsNullOrEmpty(bracket.SecondLevelEntries[i].Top.ID))
                        this.SecondLevelEntries[i].Top = teams.HashedTeams[bracket.SelectedSport][bracket.SecondLevelEntries[i].Top.ID];

                    if (!string.IsNullOrEmpty(bracket.SecondLevelEntries[i].Bottom.ID))
                        this.SecondLevelEntries[i].Bottom = teams.HashedTeams[bracket.SelectedSport][bracket.SecondLevelEntries[i].Bottom.ID];

                    this.SecondLevelEntries[i].Description = bracket.SecondLevelEntries[i].Description;
                    this.SecondLevelEntries[i].TopScore = bracket.SecondLevelEntries[i].TopScore;
                    this.SecondLevelEntries[i].BottomScore = bracket.SecondLevelEntries[i].BottomScore;
                }

                for (var i = 0; i < this.ThirdLevelEntries.Length; i++)
                {
                    if (!string.IsNullOrEmpty(bracket.ThirdLevelEntries[i].Top.ID))
                        this.ThirdLevelEntries[i].Top = teams.HashedTeams[bracket.SelectedSport][bracket.ThirdLevelEntries[i].Top.ID];

                    if (!string.IsNullOrEmpty(bracket.ThirdLevelEntries[i].Bottom.ID))
                        this.ThirdLevelEntries[i].Bottom = teams.HashedTeams[bracket.SelectedSport][bracket.ThirdLevelEntries[i].Bottom.ID];

                    this.ThirdLevelEntries[i].Description = bracket.ThirdLevelEntries[i].Description;
                    this.ThirdLevelEntries[i].TopScore = bracket.ThirdLevelEntries[i].TopScore;
                    this.ThirdLevelEntries[i].BottomScore = bracket.ThirdLevelEntries[i].BottomScore;
                }

                for (var i = 0; i < this.FourthLevelEntries.Length; i++)
                {
                    if (!string.IsNullOrEmpty(bracket.FourthLevelEntries[i].Top.ID))
                        this.FourthLevelEntries[i].Top = teams.HashedTeams[bracket.SelectedSport][bracket.FourthLevelEntries[i].Top.ID];

                    if (!string.IsNullOrEmpty(bracket.FourthLevelEntries[i].Bottom.ID))
                        this.FourthLevelEntries[i].Bottom = teams.HashedTeams[bracket.SelectedSport][bracket.FourthLevelEntries[i].Bottom.ID];

                    this.FourthLevelEntries[i].Description = bracket.FourthLevelEntries[i].Description;
                    this.FourthLevelEntries[i].TopScore = bracket.FourthLevelEntries[i].TopScore;
                    this.FourthLevelEntries[i].BottomScore = bracket.FourthLevelEntries[i].BottomScore;
                }

                for (var i = 0; i < this.FifthLevelEntries.Length; i++)
                {
                    if (!string.IsNullOrEmpty(bracket.FifthLevelEntries[i].Top.ID))
                        this.FifthLevelEntries[i].Top = teams.HashedTeams[bracket.SelectedSport][bracket.FifthLevelEntries[i].Top.ID];

                    if (!string.IsNullOrEmpty(bracket.FifthLevelEntries[i].Bottom.ID))
                        this.FifthLevelEntries[i].Bottom = teams.HashedTeams[bracket.SelectedSport][bracket.FifthLevelEntries[i].Bottom.ID];

                    this.FifthLevelEntries[i].Description = bracket.FifthLevelEntries[i].Description;
                    this.FifthLevelEntries[i].TopScore = bracket.FifthLevelEntries[i].TopScore;
                    this.FifthLevelEntries[i].BottomScore = bracket.FifthLevelEntries[i].BottomScore;
                }

                if (!string.IsNullOrEmpty(bracket.SixthLevelEntry.Top.ID))
                    this.SixthLevelEntry.Top = teams.HashedTeams[bracket.SelectedSport][bracket.SixthLevelEntry.Top.ID];

                if (!string.IsNullOrEmpty(bracket.SixthLevelEntry.Bottom.ID))
                    this.SixthLevelEntry.Bottom = teams.HashedTeams[bracket.SelectedSport][bracket.SixthLevelEntry.Bottom.ID];

                this.SixthLevelEntry.Description = bracket.SixthLevelEntry.Description;
                this.SixthLevelEntry.TopScore = bracket.SixthLevelEntry.TopScore;
                this.SixthLevelEntry.BottomScore = bracket.SixthLevelEntry.BottomScore;
            }
            catch
            {
            }
        }
    }
}