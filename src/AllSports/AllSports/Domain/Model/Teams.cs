﻿namespace AllSports.Domain.Model
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Application.Events;
    using Bracket;
    using Infrastructure.Messaging;
    using Newtonsoft.Json;

    public class Teams
    {
        private string _directory;
        private List<string> _availableSports;
        private Dictionary<string, List<string>> _availableTeams;
        private Dictionary<string, List<Team>> _teamData;
        private Dictionary<string, Dictionary<string, Team>> _hashedTeams;

        public Teams()
        {
            this._directory = string.Empty;
            this._availableSports = new List<string>();
            this._availableTeams = new Dictionary<string, List<string>>();
            this._teamData = new Dictionary<string, List<Team>>();
            this._hashedTeams = new Dictionary<string, Dictionary<string, Team>>();
        }

        public Teams(Configuration configuration)
        {
            this.UpdateOptions(configuration.ElementsDirectory);
        }

        private void HashTeams()
        {
            this._hashedTeams = new Dictionary<string, Dictionary<string, Team>>();
            foreach (var sport in this._availableSports)
            {
                if (this._availableTeams.ContainsKey(sport))
                {
                    this._hashedTeams.Add(sport, new Dictionary<string, Team>());

                    foreach (var team in this._teamData[sport])
                    {
                        this._hashedTeams[sport].Add(team.ID, team);
                    }
                }
            }
        }

        [JsonIgnore]
        public IBusMessages MessageBus { get; set; }

        public string RootDirectory => this._directory;

        public List<string> AvailableSports => this._availableSports;

        public Dictionary<string, List<string>> AvailableTeams => this._availableTeams;

        public Dictionary<string, List<Team>> TeamData => this._teamData;

        public Dictionary<string, Dictionary<string, Team>> HashedTeams => this._hashedTeams; 

        public void UpdateOptions(string rootDirectory)
        {
            this._directory = rootDirectory;
            this._availableSports = new List<string>();
            this._availableTeams = new Dictionary<string, List<string>>();
            this._teamData = new Dictionary<string, List<Team>>();
            this._hashedTeams = new Dictionary<string, Dictionary<string, Team>>();

            try
            {
                var sportDirectories = Directory.GetDirectories(rootDirectory);

                foreach (var sportDirectory in sportDirectories)
                {
                    var sportName = this.GetDirectoryName(sportDirectory);

                    this._availableSports.Add(sportName);
                    this._availableTeams.Add(sportName, new List<string>());
                    this._teamData.Add(sportName, new List<Team>());
                    this._hashedTeams.Add(sportName, new Dictionary<string, Team>());

                    foreach (var teamDirectory in Directory.GetDirectories(sportDirectory))
                    {
                        var teamName = this.GetDirectoryName(teamDirectory);

                        this._availableTeams[sportName].Add(teamName);

                        var team = new Team
                        {
                            Directory = teamDirectory,
                            Name = teamName,
                            ID = teamName // this should never change
                        };

                        this._teamData[sportName].Add(team);

                        this._hashedTeams[sportName].Add(teamName, team);
                    }
                }
            }
            catch
            {
            }
        }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public void Deserialize(string json)
        {
            try
            {
                var teams = JsonConvert.DeserializeObject<Teams>(json);
                teams.HashTeams();

                foreach (var sport in teams._availableSports)
                {
                    foreach (var team in teams._availableTeams[sport])
                    {
                        if (this._hashedTeams.ContainsKey(sport))
                        {
                            if (this._hashedTeams[sport].ContainsKey(team))
                            {
                                var teamToLoad = teams._hashedTeams[sport][team];

                                this._hashedTeams[sport][team].Name = teamToLoad.Name;
                                this._hashedTeams[sport][team].Rank = teamToLoad.Rank;
                                this._hashedTeams[sport][team].Description = teamToLoad.Description;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                this.MessageBus?.Notify(new ApplicationError(exception, "Could not deserialize teams."));
            }
        }

        private string GetDirectoryName(string path) => new DirectoryInfo(path).Name;
    }
}