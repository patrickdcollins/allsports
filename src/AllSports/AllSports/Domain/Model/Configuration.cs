﻿namespace AllSports.Domain.Model
{
    using System;
    using Events.Configuration;
    using Infrastructure.Messaging;

    public class Configuration : IDisposable
    {
        private readonly IBusMessages _messageBus;

        private string _gs2Host;
        private string _gs2Project;
        private string _elementsDirectory;

        public Configuration(IBusMessages messageBus)
        {
            this._messageBus = messageBus;

            this._gs2Host = Properties.Settings.Default.GS2Host;
            this._gs2Project = Properties.Settings.Default.GS2Project;
            this._elementsDirectory = Properties.Settings.Default.ElementsDirectory;
        }

        public string GS2Host
        {
            get { return this._gs2Host; }
            set
            {
                this._gs2Host = value;
                this.SaveSettings();
                this._messageBus.Notify(new GS2HostChanged(value));
            }
        }

        public string GS2Project
        {
            get { return this._gs2Project; }
            set
            {
                this._gs2Project = value;
                this.SaveSettings();
                this._messageBus.Notify(new GS2ProjectChanged(value));
            }
        }

        public string ElementsDirectory
        {
            get { return this._elementsDirectory; }
            set
            {
                this._elementsDirectory = value;
                this.SaveSettings();
                this._messageBus.Notify(new ElementsDirectoryChanged(value));
            }
        }

        public void Dispose()
        {
            this.SaveSettings();
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.GS2Host = this.GS2Host;
            Properties.Settings.Default.GS2Project = this.GS2Project;
            Properties.Settings.Default.ElementsDirectory = this.ElementsDirectory;

            Properties.Settings.Default.Save();
        }
    }
}
