﻿namespace AllSports.Presentation.ViewModels
{
    using Domain.Model;

    public class SettingsViewModel : BaseViewModel
    {
        private readonly Configuration _configuration;

        public SettingsViewModel(Configuration configuration)
        {
            this._configuration = configuration;
        }

        public string GS2Host
        {
            get { return this._configuration.GS2Host; }
            set
            {
                this._configuration.GS2Host = value;
                this.RaisePropertyChanged();
            }
        }

        public string GS2Project
        {
            get { return this._configuration.GS2Project; }
            set
            {
                this._configuration.GS2Project = value;
                this.RaisePropertyChanged();
            }
        }

        public string ElementsDirectory
        {
            get { return this._configuration.ElementsDirectory; }
            set
            {
                this._configuration.ElementsDirectory = value;
                this.RaisePropertyChanged();
            }
        }
    }
}
