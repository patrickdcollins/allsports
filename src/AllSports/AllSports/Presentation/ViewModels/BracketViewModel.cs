﻿namespace AllSports.Presentation.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;
    using Commands;
    using Domain.Model;
    using Domain.Model.Bracket;
    using GongSolutions.Wpf.DragDrop;
    using Handlers;
    using Infrastructure.WebServices;

    public class BracketViewModel : BaseViewModel, IDropTarget
    {
        private Bracket _bracket;

        private ObservableCollection<string> _availableSports;
        private ObservableCollection<string> _availableTeams;

        private string _selectedSport;

        private DataEngine _dataEngine;

        private Action _saveAction;

        public BracketViewModel(Teams teams)
        {
            this._bracket = new Bracket();
            this.Teams = teams;

            this._availableSports = new ObservableCollection<string>();

            this.TeamData = teams.TeamData;
            this.SportTeamMap = teams.AvailableTeams;
            this.AvailableSports = new ObservableCollection<string>(teams.AvailableSports);
            this.HashedTeams = teams.HashedTeams;

            this._dataEngine = new DataEngine();

            this.LoadFromDataEngine(teams);

            this.SaveAction = this.SaveBracketToDataEngine;
        }

        public Teams Teams { get; }

        public Bracket Bracket
        {
            get { return this._bracket; }
            set
            {
                this._bracket = value;
                this.RaisePropertyChanged();
            }
        }

        public Action SaveAction
        {
            get { return this._saveAction; }
            set
            {
                this._saveAction = value;
                this.RaisePropertyChanged();
            }
        }

        public ObservableCollection<string> AvailableSports
        {
            get { return this._availableSports; }
            set
            {
                this._availableSports = value;
                this.RaisePropertyChanged();
            }
        }

        public Dictionary<string, Dictionary<string, Team>> HashedTeams { get; set; } 

        public Dictionary<string, List<string>> SportTeamMap { get; set; }

        public Dictionary<string, List<Team>> TeamData { get; set; }

        public string SelectedSport
        {
            set
            {
                this._selectedSport = value;
                this.Bracket.SelectedSport = value;
                this.UpdateAvailableTeams(value);
            }
        }

        public ObservableCollection<string> AvailableTeams
        {
            get { return this._availableTeams; }
            set
            {
                this._availableTeams = value;
                this.RaisePropertyChanged();
            }
        }

        public DragHandler DragHandler { get; set; }

        public SaveBracketCommand SaveBracketCommand { get; set; }

        public LoadBracketCommand LoadBracketCommand { get; set; }

        private void LoadFromDataEngine(Teams teams)
        {
            var json = this._dataEngine.Read("bracket");
            if (!string.IsNullOrEmpty(json))
            {
                try
                {
                    this.Bracket.Deserialize(json, teams);
                }
                catch
                {
                }
            }
        }

        private void UpdateAvailableTeams(string selectedSport)
        {
            if (string.IsNullOrEmpty(selectedSport))
            {
                return;
            }

            if (this.SportTeamMap.ContainsKey(selectedSport))
            {
                this.AvailableTeams = new ObservableCollection<string>(this.SportTeamMap[selectedSport]);
            }
        }

        public void RefreshBracket()
        {
            this.RaisePropertyChanged("Bracket");
            this.SaveBracketToDataEngine();
        }

        public void SaveBracketToDataEngine()
        {
            this._dataEngine.Write("bracket", this.Bracket.Serialize());
        }

        public void DragOver(IDropInfo dropInfo)
        {
            dropInfo.DropTargetAdorner = DropTargetAdorners.Highlight;
            dropInfo.Effects = DragDropEffects.Copy;
        }

        public void ResetBracket()
        {
            this.Bracket = new Bracket();
            this.Bracket.SelectedSport = this._selectedSport;
            this.SaveBracketToDataEngine();
        }

        public void Drop(IDropInfo dropInfo)
        {
            var selectedTeamID = dropInfo.Data as string;
            if (!string.IsNullOrEmpty(selectedTeamID))
            {
                if (!this.AvailableTeams.Contains(selectedTeamID))
                {
                    try
                    {
                        var sourceTargetData = selectedTeamID.Split('_');
                        var isTop = sourceTargetData[0] == "top";
                        var bracketLevelIndex = Convert.ToInt32(sourceTargetData[1]);
                        var entryLevelIndex = Convert.ToInt32(sourceTargetData[2]);

                        Entry sourceEntry = null;
                        switch (bracketLevelIndex)
                        {
                            case 0:
                                sourceEntry = this.Bracket.FirstLevelEntries[entryLevelIndex];
                                break;
                            case 1:
                                sourceEntry = this.Bracket.SecondLevelEntries[entryLevelIndex];
                                break;
                            case 2:
                                sourceEntry = this.Bracket.ThirdLevelEntries[entryLevelIndex];
                                break;
                            case 3:
                                sourceEntry = this.Bracket.FourthLevelEntries[entryLevelIndex];
                                break;
                            case 4:
                                sourceEntry = this.Bracket.FifthLevelEntries[entryLevelIndex];
                                break;
                            case 5:
                                sourceEntry = this.Bracket.SixthLevelEntry;
                                break;
                        }

                        if (sourceEntry != null)
                        {
                            if (isTop)
                            {
                                selectedTeamID = sourceEntry.Top.ID;
                            }
                            else
                            {
                                selectedTeamID = sourceEntry.Bottom.ID;
                            }
                        }
                    }
                    catch
                    {
                        return;
                    }
                }

                if (this.AvailableTeams.Contains(selectedTeamID))
                {
                    var droppedOverTarget = dropInfo.VisualTarget as Border;
                    if (droppedOverTarget != null)
                    {
                        var name = droppedOverTarget.Tag as string;

                        try
                        {
                            var dropTargetData = name.Split('_');
                            var isTop = dropTargetData[0] == "top";
                            var bracketLevelIndex = Convert.ToInt32(dropTargetData[1]);
                            var entryLevelIndex = Convert.ToInt32(dropTargetData[2]);

                            Entry targetEntry = null;
                            switch (bracketLevelIndex)
                            {
                                case 0:
                                    targetEntry = this.Bracket.FirstLevelEntries[entryLevelIndex];
                                    break;
                                case 1:
                                    targetEntry = this.Bracket.SecondLevelEntries[entryLevelIndex];
                                    break;
                                case 2:
                                    targetEntry = this.Bracket.ThirdLevelEntries[entryLevelIndex];
                                    break;
                                case 3:
                                    targetEntry = this.Bracket.FourthLevelEntries[entryLevelIndex];
                                    break;
                                case 4:
                                    targetEntry = this.Bracket.FifthLevelEntries[entryLevelIndex];
                                    break;
                                case 5:
                                    targetEntry = this.Bracket.SixthLevelEntry;
                                    break;
                            }

                            if (targetEntry != null)
                            {
                                if (isTop)
                                {
                                    targetEntry.Top = this.HashedTeams[this._selectedSport][selectedTeamID];
                                }
                                else
                                {
                                    targetEntry.Bottom = this.HashedTeams[this._selectedSport][selectedTeamID];
                                }

                                this.RefreshBracket();
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }
    }
}