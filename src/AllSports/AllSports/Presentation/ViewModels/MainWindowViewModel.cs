﻿namespace AllSports.Presentation.ViewModels
{
    using Commands;

    public class MainWindowViewModel : BaseViewModel
    {
        private string _status;

        private TeamsViewModel _teamsViewModel;
        private BracketViewModel _bracketViewModel;
        private SettingsViewModel _settingsViewModel;

        public string Status
        {
            get { return this._status; }
            set
            {
                this._status = value;
                this.RaisePropertyChanged();
            }
        }

        public TeamsViewModel TeamsViewModel
        {
            get { return this._teamsViewModel; }
            set
            {
                this._teamsViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        public BracketViewModel BracketViewModel
        {
            get { return this._bracketViewModel; }
            set
            {
                this._bracketViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        public SettingsViewModel SettingsViewModel
        {
            get { return this._settingsViewModel; }
            set
            {
                this._settingsViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        public LaunchProjectCommand LaunchProjectCommand { get; set; }
    }
}