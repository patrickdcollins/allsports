﻿namespace AllSports.Presentation.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Commands;
    using Domain.Model;
    using Domain.Model.Bracket;
    using Infrastructure.WebServices;

    public class TeamsViewModel : BaseViewModel
    {
        private readonly Teams _teams;

        private BracketViewModel _bracketViewModel;

        private ObservableCollection<string> _availableSports;
        private ObservableCollection<string> _availableTeams;

        private string _selectedSport;
        private string _selectedTeamID;

        private string _teamLogoPath;
        private string _teamID;

        private string _selectedTeamName;
        private string _selectedTeamRank;
        private string _selectedTeamDescription;

        private DataEngine _dataEngine;

        public TeamsViewModel(Teams teams, BracketViewModel bracketViewModel)
        {
            this._teams = teams;
            this._bracketViewModel = bracketViewModel;

            this._availableSports = new ObservableCollection<string>();
            this.SportTeamMap = new Dictionary<string, List<string>>();

            this.TeamData = teams.TeamData;
            this.SportTeamMap = teams.AvailableTeams;
            this.AvailableSports = new ObservableCollection<string>(teams.AvailableSports);

            this._dataEngine = new DataEngine();

            this.LoadFromDataEngine();
        }

        public ObservableCollection<string> AvailableSports
        {
            get { return this._availableSports; }
            set
            {
                this._availableSports = value;
                this.RaisePropertyChanged();
            }
        }

        public Dictionary<string, List<string>> SportTeamMap { get; set; }

        public Dictionary<string, List<Team>> TeamData { get; set; }

        public string SelectedSport
        {
            set
            {
                this._selectedSport = value;
                this.UpdateAvailableTeams(value);
            }
        }

        public ObservableCollection<string> AvailableTeams
        {
            get { return this._availableTeams; }
            set
            {
                this._availableTeams = value;
                this.RaisePropertyChanged();
            }
        }

        public string SelectedTeam
        {
            set
            {
                this._selectedTeamID = value;
                this.UpdateTeamInfo(value);
            }
        }

        public string TeamLogoPath
        {
            get { return this._teamLogoPath; }
            set
            {
                this._teamLogoPath = value;
                this.RaisePropertyChanged();
            }
        }

        public string TeamID
        {
            get { return this._teamID; }
            set
            {
                this._teamID = value;
                this.RaisePropertyChanged();
            }
        }

        public string TeamName
        {
            get { return this._selectedTeamName; }
            set
            {
                this._selectedTeamName = value;
                this.RaisePropertyChanged();
                this.SaveTeamInfo();
            }
        }

        public string TeamRank
        {
            get { return this._selectedTeamRank; }
            set
            {
                this._selectedTeamRank = value;
                this.RaisePropertyChanged();
                this.SaveTeamInfo();
            }
        }

        public string TeamDescription
        {
            get { return this._selectedTeamDescription; }
            set
            {
                this._selectedTeamDescription = value;
                this.RaisePropertyChanged();
                this.SaveTeamInfo();
            }
        }

        public SaveTeamsCommand SaveTeamsCommand { get; set; }

        public LoadTeamsCommand LoadTeamsCommand { get; set; }

        private void LoadFromDataEngine()
        {
            var json = this._dataEngine.Read("teams");
            if (!string.IsNullOrEmpty(json))
            {
                try
                {
                    this._teams.Deserialize(json);
                }
                catch
                {
                }
            }
        }

        private void UpdateAvailableTeams(string selectedSport)
        {
            if (string.IsNullOrEmpty(selectedSport))
            {
                return;
            }

            if (this.SportTeamMap.ContainsKey(selectedSport))
            {
                this.AvailableTeams = new ObservableCollection<string>(this.SportTeamMap[selectedSport]);
            }
        }

        private void UpdateTeamInfo(string selectedTeam)
        {
            if (string.IsNullOrEmpty(selectedTeam))
            {
                this.ClearTeamData();
                return;
            }

            this.ValidateTeamData(selectedTeam);
        }

        private void ValidateTeamData(string selectedTeam)
        {
            if (string.IsNullOrEmpty(this._selectedSport))
            {
                return;
            }

            if (this.TeamData.ContainsKey(this._selectedSport))
            {
                var teamsData = this.TeamData[this._selectedSport];

                Team team = null;
                foreach (var teamData in teamsData)
                {
                    if (teamData.ID == selectedTeam)
                    {
                        team = teamData;
                        break;
                    }
                }

                if (team == null)
                {
                    this.ClearTeamData();
                    return;
                }

                this.PopulateTeamData(team);
            }
            else
            {
                this.ClearTeamData();
            }
        }

        private void ClearTeamData()
        {
            this.TeamLogoPath = string.Empty;
            this.TeamID = string.Empty;

            this._selectedTeamName = string.Empty;
            this._selectedTeamRank = string.Empty;
            this._selectedTeamDescription = string.Empty;

            this.RaisePropertyChanged("TeamName");
            this.RaisePropertyChanged("TeamRank");
            this.RaisePropertyChanged("TeamDescription");
        }

        private void PopulateTeamData(Team team)
        {
            this.TeamLogoPath = team.LogoPath;
            this.TeamID = team.ID;

            this._selectedTeamName = team.Name;
            this._selectedTeamRank = team.Rank;
            this._selectedTeamDescription = team.Description;

            this.RaisePropertyChanged("TeamName");
            this.RaisePropertyChanged("TeamRank");
            this.RaisePropertyChanged("TeamDescription");
        }

        public void RefreshTeamData()
        {
            this.UpdateTeamInfo(this._selectedTeamID);
            this.SaveTeamsToDataEngine();
        }

        public void SaveTeamsToDataEngine()
        {
            this._dataEngine.Write("teams", this._teams.Serialize());
        }

        private void SaveTeamInfo()
        {
            if (string.IsNullOrEmpty(this._selectedSport) || string.IsNullOrEmpty(this._selectedTeamID))
            {
                return;
            }

            if (this.TeamData.ContainsKey(this._selectedSport))
            {
                foreach (var team in this.TeamData[this._selectedSport])
                {
                    if (team.ID == this._selectedTeamID)
                    {
                        team.Name = this._selectedTeamName;
                        team.Rank = this._selectedTeamRank;
                        team.Description = this._selectedTeamDescription;

                        this._bracketViewModel.RefreshBracket();

                        return;
                    }
                }
            }
        }
    }
}