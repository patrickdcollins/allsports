﻿namespace AllSports.Presentation.ViewModels
{
    using System.ComponentModel;
    using System.Diagnostics;

    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged()
        {
            var propertyName = new StackTrace().GetFrame(1).GetMethod().Name.Substring(4);

            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}