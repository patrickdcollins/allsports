﻿namespace AllSports.Presentation.Handlers
{
    using System.Windows;
    using System.Windows.Controls;
    using GongSolutions.Wpf.DragDrop;

    public class DragHandler : IDragSource
    {
        public bool CanStartDrag(IDragInfo dragInfo) => true;

        public void DragCancelled()
        {
        }

        public void Dropped(IDropInfo dropInfo)
        {
        }

        public void StartDrag(IDragInfo dragInfo)
        {
            var draggedTeamID = dragInfo.SourceItem as string;
            if (!string.IsNullOrEmpty(draggedTeamID))
            {
                dragInfo.Data = draggedTeamID;
                dragInfo.Effects = DragDropEffects.Copy;
            }
            else
            {
                var draggedBorder = dragInfo.VisualSource as Border;
                var draggedTag = draggedBorder?.Tag as string;
                if (!string.IsNullOrEmpty(draggedTag))
                {
                    dragInfo.Data = draggedTag;
                    dragInfo.Effects = DragDropEffects.Copy;
                }
            }
        }
    }
}
