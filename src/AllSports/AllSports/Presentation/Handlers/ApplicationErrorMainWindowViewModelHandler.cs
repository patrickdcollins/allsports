﻿namespace AllSports.Presentation.Handlers
{
    using Application.Events;
    using Infrastructure.Messaging;
    using ViewModels;

    public class ApplicationErrorMainWindowViewModelHandler : IHandleMessages<ApplicationError>
    {
        private readonly MainWindowViewModel _mainWindowViewModel;

        public ApplicationErrorMainWindowViewModelHandler(MainWindowViewModel mainWindowViewModel)
        {
            this._mainWindowViewModel = mainWindowViewModel;
        }

        public IBusMessages Bus { get; set; }

        public void Handle(ApplicationError message)
        {
            if (!string.IsNullOrEmpty(message.Message))
            {
                this._mainWindowViewModel.Status = message.Message;
            }
        }
    }
}