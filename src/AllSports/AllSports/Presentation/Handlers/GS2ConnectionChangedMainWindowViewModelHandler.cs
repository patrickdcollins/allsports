﻿namespace AllSports.Presentation.Handlers
{
    using Infrastructure.Events;
    using Infrastructure.Messaging;
    using ViewModels;

    public class GS2ConnectionChangedMainWindowViewModelHandler : IHandleMessages<GS2ConnectionChanged>
    {
        private readonly MainWindowViewModel _mainWindowViewModel;

        public GS2ConnectionChangedMainWindowViewModelHandler(MainWindowViewModel mainWindowViewModel)
        {
            this._mainWindowViewModel = mainWindowViewModel;
        }

        public IBusMessages Bus { get; set; }

        public void Handle(GS2ConnectionChanged message)
        {
            this._mainWindowViewModel.Status = message.IsConnected
                ? "Project started."
                : "GS2 not connected!";
        }
    }
}