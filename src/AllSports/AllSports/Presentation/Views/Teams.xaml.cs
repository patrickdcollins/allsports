﻿namespace AllSports.Presentation.Views
{
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Forms;
    using Commands.CommandParameters;
    using ViewModels;
    using UserControl = System.Windows.Controls.UserControl;

    public partial class Teams : UserControl
    {
        public Teams()
        {
            this.InitializeComponent();
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var viewModel = this.DataContext as TeamsViewModel;
            if (viewModel != null)
            {
                var saveDialog = new SaveFileDialog();

                saveDialog.Filter = "JSON file (*.json)|*.json";

                var dialogResult = saveDialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    viewModel.SaveTeamsCommand.Execute(new SaveTeamsCommandParameters
                    {
                        FilePath = saveDialog.FileName
                    });
                }
            }
        }

        private void LoadButton_OnClick(object sender, RoutedEventArgs e)
        {
            var viewModel = this.DataContext as TeamsViewModel;
            if (viewModel != null)
            {
                var loadDialog = new OpenFileDialog();

                loadDialog.Filter = "JSON file (*.json)|*.json";

                var dialogResult = loadDialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    viewModel.LoadTeamsCommand.Execute(new LoadTeamsCommandParameters
                    {
                        FilePath = loadDialog.FileName
                    });

                    viewModel.RefreshTeamData();
                }
            }
        }

        private void Textbox_OnSourceUpdated(object sender, DataTransferEventArgs e)
        {
            var viewModel = this.DataContext as TeamsViewModel;
            viewModel?.SaveTeamsToDataEngine();
        }
    }
}