﻿namespace AllSports.Presentation.Views
{
    using System.Windows;
    using System.Windows.Forms;
    using ViewModels;
    using UserControl = System.Windows.Controls.UserControl;

    public partial class Settings : UserControl
    {
        public Settings()
        {
            this.InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                var viewModel = this.DataContext as SettingsViewModel;

                if (viewModel != null)
                {
                    viewModel.ElementsDirectory = dialog.SelectedPath;
                }
            }
        }
    }
}