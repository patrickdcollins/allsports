﻿namespace AllSports.Presentation.Views
{
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Forms;
    using Commands.CommandParameters;
    using ViewModels;
    using UserControl = System.Windows.Controls.UserControl;

    public partial class Bracket : UserControl
    {
        public Bracket()
        {
            this.InitializeComponent();
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var viewModel = this.DataContext as BracketViewModel;
            if (viewModel != null)
            {
                var saveDialog = new SaveFileDialog();

                saveDialog.Filter = "JSON file (*.json)|*.json";

                var dialogResult = saveDialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    viewModel.SaveBracketCommand.Execute(new SaveBracketCommandParameters
                    {
                        FilePath = saveDialog.FileName,
                        Bracket = viewModel.Bracket
                    });
                }
            }
        }

        private void LoadButton_OnClick(object sender, RoutedEventArgs e)
        {
            var viewModel = this.DataContext as BracketViewModel;
            if (viewModel != null)
            {
                var loadDialog = new OpenFileDialog();

                loadDialog.Filter = "JSON file (*.json)|*.json";

                var dialogResult = loadDialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    viewModel.LoadBracketCommand.Execute(new LoadBracketCommandParameters
                    {
                        FilePath = loadDialog.FileName,
                        Bracket = viewModel.Bracket,
                        Teams = viewModel.Teams
                    });

                    viewModel.RefreshBracket();
                }
            }
        }

        private void ClearButton_OnClick(object sender, RoutedEventArgs e)
        {
            var result = System.Windows.MessageBox.Show("WARNING: This will clear out all bracket entries!", "Clear Bracket", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                var viewModel = this.DataContext as BracketViewModel;
                viewModel?.ResetBracket();
            }
        }

        private void FrameworkElement_OnSourceUpdated(object sender, DataTransferEventArgs e)
        {
            var viewModel = this.DataContext as BracketViewModel;
            viewModel?.SaveBracketToDataEngine();
        }

        private void ChooseSponsorButton_OnClick(object sender, RoutedEventArgs e)
        {
            var viewModel = this.DataContext as BracketViewModel;
            if (viewModel != null)
            {
                var loadDialog = new OpenFileDialog();

                loadDialog.Filter = "PNG file (*.png)|*.png|JPG file (*.jpg)|*.jpg|All files (*.*)|*.*";

                var dialogResult = loadDialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    viewModel.Bracket.SponsorFilePath = loadDialog.FileName;

                    viewModel.RefreshBracket();
                }
            }
        }
    }
}