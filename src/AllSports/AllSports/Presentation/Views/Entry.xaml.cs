﻿namespace AllSports.Presentation.Views
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using GongSolutions.Wpf.DragDrop;
    using Handlers;

    public partial class Entry : UserControl
    {
        public Entry()
        {
            this.InitializeComponent();
        }

        public DragHandler DragHandler { get; set; } = new DragHandler();

        public string Description
        {
            get { return this.GetValue(Entry.DescriptionProperty) as string; }
            set { this.SetValue(Entry.DescriptionProperty, value); }
        }

        public string TopScore
        {
            get { return this.GetValue(Entry.TopScoreProperty) as string; }
            set { this.SetValue(Entry.TopScoreProperty, value); }
        }

        public string BottomScore
        {
            get { return this.GetValue(Entry.BottomScoreProperty) as string; }
            set { this.SetValue(Entry.BottomScoreProperty, value); }
        }

        public string TopFilePath
        {
            get { return this.GetValue(Entry.TopFilePathProperty) as string; }
            set { this.SetValue(Entry.TopFilePathProperty, value); }
        }

        public string BottomFilePath
        {
            get { return this.GetValue(Entry.BottomFilePathProperty) as string; }
            set { this.SetValue(Entry.BottomFilePathProperty, value); }
        }

        public string TopTeamID
        {
            get { return this.GetValue(Entry.TopTeamIDProperty) as string; }
            set { this.SetValue(Entry.TopTeamIDProperty, value); }
        }

        public string BottomTeamID
        {
            get { return this.GetValue(Entry.BottomTeamIDProperty) as string; }
            set { this.SetValue(Entry.BottomTeamIDProperty, value); }
        }

        public IDropTarget DropHandler
        {
            get { return this.GetValue(Entry.DropHandlerProperty) as IDropTarget; }
            set { this.SetValue(Entry.DropHandlerProperty, value); }
        }

        public Action OnInternalPropertyUpdated
        {
            get { return this.GetValue(Entry.OnInternalPropertyUpdatedProperty) as Action; }
            set { this.SetValue(Entry.OnInternalPropertyUpdatedProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(Entry), new PropertyMetadata(null));

        public static readonly DependencyProperty TopScoreProperty =
            DependencyProperty.Register("TopScore", typeof(string), typeof(Entry), new PropertyMetadata(null));

        public static readonly DependencyProperty BottomScoreProperty =
            DependencyProperty.Register("BottomScore", typeof(string), typeof(Entry), new PropertyMetadata(null));

        public static readonly DependencyProperty TopFilePathProperty =
            DependencyProperty.Register("TopFilePath", typeof(string), typeof(Entry), new PropertyMetadata(null));

        public static readonly DependencyProperty BottomFilePathProperty =
            DependencyProperty.Register("BottomFilePath", typeof(string), typeof(Entry), new PropertyMetadata(null));

        public static readonly DependencyProperty TopTeamIDProperty =
            DependencyProperty.Register("TopTeamID", typeof(string), typeof(Entry), new PropertyMetadata(null));

        public static readonly DependencyProperty BottomTeamIDProperty =
            DependencyProperty.Register("BottomTeamID", typeof(string), typeof(Entry), new PropertyMetadata(null));

        public static readonly DependencyProperty DropHandlerProperty =
            DependencyProperty.Register("DropHandler", typeof(IDropTarget), typeof(Entry), new PropertyMetadata(null));

        public static readonly DependencyProperty OnInternalPropertyUpdatedProperty =
            DependencyProperty.Register("OnInternalPropertyUpdated", typeof(Action), typeof(Entry), new PropertyMetadata(null));

        private void FrameworkElement_OnSourceUpdated(object sender, DataTransferEventArgs e)
        {
            this.OnInternalPropertyUpdated?.Invoke();
        }
    }
}