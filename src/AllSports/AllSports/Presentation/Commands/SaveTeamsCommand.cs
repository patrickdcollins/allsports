﻿namespace AllSports.Presentation.Commands
{
    using System;
    using System.IO;
    using System.Windows.Input;
    using CommandParameters;
    using Domain.Model;

    public class SaveTeamsCommand : ICommand
    {
        private readonly Teams _teams;

        public SaveTeamsCommand(Teams teams)
        {
            this._teams = teams;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            var parameters = parameter as SaveTeamsCommandParameters;
            if (parameters != null)
            {
                try
                {
                    var json = this._teams.Serialize();
                    File.WriteAllText(parameters.FilePath, json);
                }
                catch
                {
                }
            }
        }
    }
}