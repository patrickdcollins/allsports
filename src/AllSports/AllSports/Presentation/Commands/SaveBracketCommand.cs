﻿namespace AllSports.Presentation.Commands
{
    using System;
    using System.IO;
    using System.Windows.Input;
    using CommandParameters;

    public class SaveBracketCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            var parameters = parameter as SaveBracketCommandParameters;
            if (parameters != null)
            {
                try
                {
                    var json = parameters.Bracket.Serialize();
                    File.WriteAllText(parameters.FilePath, json);
                }
                catch
                {
                }
            }
        }
    }
}