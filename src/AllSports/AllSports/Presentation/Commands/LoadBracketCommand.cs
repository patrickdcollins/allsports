﻿namespace AllSports.Presentation.Commands
{
    using System;
    using System.IO;
    using System.Windows.Input;
    using CommandParameters;

    public class LoadBracketCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            var parameters = parameter as LoadBracketCommandParameters;
            if (parameters != null)
            {
                try
                {
                    var json = File.ReadAllText(parameters.FilePath);
                    parameters.Bracket.Deserialize(json, parameters.Teams);
                }
                catch
                {
                }
            }
        }
    }
}