﻿namespace AllSports.Presentation.Commands.CommandParameters
{
    using Domain.Model;
    using Domain.Model.Bracket;

    public class LoadBracketCommandParameters
    {
        public string FilePath { get; set; } = string.Empty;

        public Bracket Bracket { get; set; } = new Bracket();

        public Teams Teams { get; set; } = new Teams();
    }
}