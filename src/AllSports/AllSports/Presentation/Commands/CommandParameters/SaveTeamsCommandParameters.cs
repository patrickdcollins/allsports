﻿namespace AllSports.Presentation.Commands.CommandParameters
{
    public class SaveTeamsCommandParameters
    {
        public string FilePath { get; set; } = string.Empty;
    }
}
