﻿namespace AllSports.Presentation.Commands.CommandParameters
{
    using Domain.Model.Bracket;

    public class SaveBracketCommandParameters
    {
        public string FilePath { get; set; } = string.Empty;

        public Bracket Bracket { get; set; } = new Bracket();
    }
}