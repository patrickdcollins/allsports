﻿namespace AllSports.Presentation.Commands.CommandParameters
{
    public class LoadTeamsCommandParameters
    {
        public string FilePath { get; set; } = string.Empty;
    }
}
