﻿namespace AllSports.Presentation.Commands
{
    using System;
    using System.Windows.Input;
    using Infrastructure.Playout;

    public class LaunchProjectCommand : ICommand
    {
        private readonly GS2Controller _gs2Controller;

        public LaunchProjectCommand(GS2Controller gs2Controller)
        {
            this._gs2Controller = gs2Controller;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            if (!this._gs2Controller.IsConnected)
            {
                this._gs2Controller.Connect();
            }
        }
    }
}