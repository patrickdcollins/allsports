﻿namespace AllSports.Presentation.Commands
{
    using System;
    using System.IO;
    using System.Windows.Input;
    using CommandParameters;
    using Domain.Model;

    public class LoadTeamsCommand : ICommand
    {
        private readonly Teams _teams;

        public LoadTeamsCommand(Teams teams)
        {
            this._teams = teams;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            var parameters = parameter as LoadTeamsCommandParameters;
            if (parameters != null)
            {
                try
                {
                    var json = File.ReadAllText(parameters.FilePath);
                    this._teams.Deserialize(json);
                }
                catch
                {
                }
            }
        }
    }
}
