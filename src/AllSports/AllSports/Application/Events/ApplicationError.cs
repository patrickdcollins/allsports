﻿namespace AllSports.Application.Events
{
    using System;
    using Infrastructure.Messaging;

    public class ApplicationError : IApplicationEvent
    {
        public ApplicationError(Exception exception)
        {
            this.Exception = exception;
            this.Message = string.Empty;
        }

        public ApplicationError(string message)
        {
            this.Exception = new Exception();
            this.Message = message;
        }

        public ApplicationError(Exception exception, string message)
        {
            this.Exception = exception;
            this.Message = message;
        }

        public Exception Exception { get; }

        public string Message { get; }
    }
}