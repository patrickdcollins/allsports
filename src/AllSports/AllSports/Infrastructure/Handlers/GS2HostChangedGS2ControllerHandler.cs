﻿namespace AllSports.Infrastructure.Handlers
{
    using Domain.Events.Configuration;
    using Messaging;
    using Playout;

    public class GS2HostChangedGS2ControllerHandler : IHandleMessages<GS2HostChanged>
    {
        private readonly GS2Controller _gs2Controller;

        public GS2HostChangedGS2ControllerHandler(GS2Controller gs2Controller)
        {
            this._gs2Controller = gs2Controller;
        }

        public IBusMessages Bus { get; set; }

        public void Handle(GS2HostChanged message)
        {
            this._gs2Controller.GS2Host = message.GS2Host;
        }
    }
}