﻿namespace AllSports.Infrastructure.Handlers
{
    using Domain.Events.Configuration;
    using Messaging;
    using Playout;

    public class GS2ProjectChangedGS2ControllerHandler : IHandleMessages<GS2ProjectChanged>
    {
        private readonly GS2Controller _gs2Controller;

        public GS2ProjectChangedGS2ControllerHandler(GS2Controller gs2Controller)
        {
            this._gs2Controller = gs2Controller;
        }

        public IBusMessages Bus { get; set; }

        public void Handle(GS2ProjectChanged message)
        {
            this._gs2Controller.GS2Project = message.GS2Project;
        }
    }
}