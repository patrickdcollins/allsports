﻿namespace AllSports.Infrastructure.Playout
{
    using System;
    using Application.Events;
    using Domain.Model;
    using Events;
    using GSTK5Lib;
    using Messaging;

    public class GS2Controller
    {
        private bool _isConnected;

        public GS2Controller(Configuration configuration)
        {
            this.GS2Host = configuration.GS2Host;
            this.GS2Project = configuration.GS2Project;
        }

        public IBusMessages MessageBus { get; set; }

        public string GS2Host { get; set; }

        public string GS2Project { get; set; }

        public IGSTitleManager TM { get; set; }

        public bool IsConnected
        {
            get { return this._isConnected; }
            set
            {
                this._isConnected = value;
                this.MessageBus?.Notify(new GS2ConnectionChanged(value));
            }
        }

        public void Connect()
        {
            try
            {
                this.TM = new TitleManager();
                this.TM.setProject(this.GS2Project);

                var client = this.TM.getClient();
                client.setServerAddress(this.GS2Host);
                client.connect();
                client.onMessage += this.ClientOnMessage;
                client.roaSet("::Output", "mousePickOverride", true);

                this.IsConnected = true;
            }
            catch (Exception exception)
            {
                this.MessageBus?.Notify(new ApplicationError(exception, "GS2Controller could not connect to GS2."));

                this.IsConnected = false;
            }
        }

        private void ClientOnMessage(string message)
        {
            this.MessageBus?.Notify(new GS2MessageReceived(message));
        }
    }
}
