﻿namespace AllSports.Infrastructure.DependencyInjection.Modules
{
    using System;
    using Autofac;
    using Domain.Events.Configuration;
    using Domain.Handlers;
    using Domain.Model;
    using Messaging;

    public class DomainLayerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Configuration>().AsSelf().As<IDisposable>().SingleInstance().AutoActivate();

            builder.RegisterType<Teams>().AsSelf().SingleInstance().PropertiesAutowired().AutoActivate().UsingConstructor(typeof(Configuration));

            // Handlers

            builder.RegisterType<ElementsDirectoryChangedTeamsHandler>()
                .As<IHandleMessages<ElementsDirectoryChanged>>()
                .PropertiesAutowired();
        }
    }
}