﻿namespace AllSports.Infrastructure.DependencyInjection.Modules
{
    using Application.Events;
    using Autofac;
    using Domain.Events;
    using Events;
    using Messaging;
    using Presentation.Commands;
    using Presentation.Handlers;
    using Presentation.ViewModels;
    using Presentation.Views;

    public class PresentationLayerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Views

            builder.RegisterType<MainWindow>().AsSelf().SingleInstance();

            // ViewModels

            builder.RegisterType<MainWindowViewModel>()
                .AsSelf()
                .SingleInstance()
                .PropertiesAutowired();

            builder.RegisterType<TeamsViewModel>()
                .AsSelf()
                .SingleInstance()
                .PropertiesAutowired();

            builder.RegisterType<BracketViewModel>()
                .AsSelf()
                .SingleInstance()
                .PropertiesAutowired();

            builder.RegisterType<SettingsViewModel>()
                .AsSelf()
                .SingleInstance()
                .PropertiesAutowired();

            // Handlers
            
            builder.RegisterType<GS2ConnectionChangedMainWindowViewModelHandler>()
                .As<IHandleMessages<GS2ConnectionChanged>>()
                .PropertiesAutowired();

            builder.RegisterType<ApplicationErrorMainWindowViewModelHandler>()
                .As<IHandleMessages<ApplicationError>>()
                .PropertiesAutowired();

            builder.RegisterType<DragHandler>()
                .AsSelf()
                .PropertiesAutowired();

            // Commands

            builder.RegisterType<LaunchProjectCommand>().AsSelf().PropertiesAutowired();

            builder.RegisterType<SaveTeamsCommand>().AsSelf().PropertiesAutowired();

            builder.RegisterType<LoadTeamsCommand>().AsSelf().PropertiesAutowired();

            builder.RegisterType<SaveBracketCommand>().AsSelf().PropertiesAutowired();

            builder.RegisterType<LoadBracketCommand>().AsSelf().PropertiesAutowired();
        }
    }
}