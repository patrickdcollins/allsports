﻿namespace AllSports.Infrastructure.DependencyInjection.Modules
{
    using Autofac;
    using Domain.Events.Configuration;
    using Handlers;
    using Messaging;
    using Playout;

    public class InfrastructureLayerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new ObjectResolver(c.Resolve<IComponentContext>()))
                .As<IResolveObjects>()
                .SingleInstance();

            builder.RegisterType<MessageBus>().As<IBusMessages>().SingleInstance().PropertiesAutowired();

            builder.RegisterType<GS2Controller>().AsSelf().SingleInstance().PropertiesAutowired();

            // Handlers

            builder.RegisterType<GS2ProjectChangedGS2ControllerHandler>()
                .As<IHandleMessages<GS2ProjectChanged>>()
                .PropertiesAutowired();

            builder.RegisterType<GS2HostChangedGS2ControllerHandler>()
                .As<IHandleMessages<GS2HostChanged>>()
                .PropertiesAutowired();
        }
    }
}