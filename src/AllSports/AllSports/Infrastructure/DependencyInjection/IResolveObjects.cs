﻿namespace AllSports.Infrastructure.DependencyInjection
{
    using System.Collections.Generic;

    public interface IResolveObjects
    {
        T Get<T>();

        IEnumerable<T> GetAll<T>();
    }
}