﻿namespace AllSports.Infrastructure.DependencyInjection
{
    using System.Collections.Generic;
    using Autofac;

    public class ObjectResolver : IResolveObjects
    {
        private readonly IComponentContext _componentContext;

        public ObjectResolver(IComponentContext componentContext)
        {
            this._componentContext = componentContext;
        }

        public T Get<T>()
        {
            return this._componentContext.Resolve<T>();
        }

        public IEnumerable<T> GetAll<T>()
        {
            return this._componentContext.Resolve<IEnumerable<T>>();
        }
    }
}