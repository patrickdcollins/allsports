﻿namespace AllSports.Infrastructure.Events
{
    using Messaging;

    public class GS2MessageReceived : IInfrastructureEvent
    {
        public GS2MessageReceived(string message)
        {
            this.Message = message;
        }

        public string Message { get; }
    }
}