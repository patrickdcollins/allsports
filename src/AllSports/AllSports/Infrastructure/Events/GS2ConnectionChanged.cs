﻿namespace AllSports.Infrastructure.Events
{
    using Messaging;

    public class GS2ConnectionChanged : IInfrastructureEvent
    {
        public GS2ConnectionChanged(bool isConnected)
        {
            this.IsConnected = isConnected;
        }

        public bool IsConnected { get; }
    }
}