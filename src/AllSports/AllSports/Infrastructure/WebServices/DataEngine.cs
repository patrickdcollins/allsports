﻿namespace AllSports.Infrastructure.WebServices
{
    using System.Net;

    public class DataEngine
    {
        private readonly string _requestFormat = "http://{0}:{1}/_data/{2}/{3}";

        private string _host = "localhost";
        private int _port = 4300;

        private string _bucket = "AllSports";

        public string Read(string key)
        {
            try
            {
                var url = string.Format(this._requestFormat, this._host, this._port, this._bucket, key);

                using (var client = new WebClient())
                {
                    return client.DownloadString(url);
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        public void Write(string key, string json)
        {
            try
            {
                var url = string.Format(this._requestFormat, this._host, this._port, this._bucket, key);

                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.UploadString(url, "POST", json);
                }
            }
            catch
            {
            }
        }
    }
}
