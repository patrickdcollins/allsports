﻿namespace AllSports.Infrastructure.Messaging
{
    public interface IHandleMessages<T> where T : IMessage
    {
        IBusMessages Bus { get; set; }

        void Handle(T message);
    }
}