﻿namespace AllSports.Infrastructure.Messaging
{
    using System.Windows;
    using DependencyInjection;

    public class MessageBus : IBusMessages
    {
        private readonly IResolveObjects _objectResolver;

        public MessageBus(IResolveObjects objectResolver)
        {
            this._objectResolver = objectResolver;
        }

        public void Notify<T>(T message) where T : IMessage
        {
            MessageBus.HandleMessage(this._objectResolver, message);
        }

        private static void HandleMessage<T>(IResolveObjects objectResolver, T message) where T : IMessage
        {
            try
            {
                foreach (var handler in objectResolver.GetAll<IHandleMessages<T>>())
                {
                    Application.Current.Dispatcher.Invoke(() => { handler.Handle(message); });
                }
            }
            catch
            {
            }
        }
    }
}