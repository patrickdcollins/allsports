﻿namespace AllSports
{
    using System.Windows;
    using Autofac;
    using Infrastructure.DependencyInjection.Modules;
    using Presentation.ViewModels;
    using Presentation.Views;

    public partial class App : System.Windows.Application
    {
        private IContainer _container;

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            this.InitAutofacContainer();

            var mainWindow = this._container.Resolve<MainWindow>();
            mainWindow.DataContext = this._container.Resolve<MainWindowViewModel>();
            mainWindow.Show();
        }

        private void InitAutofacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new InfrastructureLayerModule());
            builder.RegisterModule(new ApplicationLayerModule());
            builder.RegisterModule(new DomainLayerModule());
            builder.RegisterModule(new PresentationLayerModule());
            this._container = builder.Build();
        }

        private void App_OnExit(object sender, ExitEventArgs e)
        {
            this._container.Dispose();
        }
    }
}